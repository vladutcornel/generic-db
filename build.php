<?php

include 'ClientSide.php';

file_put_contents(__DIR__.'/dist/GenericDB.js', ClientSide::merge(array(
    __DIR__.'/src/main.js',
    __DIR__.'/src/ajax.js',
    __DIR__.'/src/indexed.js',
    __DIR__.'/src/websql.js',
)));