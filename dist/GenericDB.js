
/*********************************************************************************
 * main.js
 *********************************************************************************/

/**
 * A generic database
 * @param {function} callback The function called when the database is ready
 * @returns {GenericDB}
 */
function GenericDB(callback) {
// Force this call to be treated as a constructor
    if (!(this instanceof GenericDB))
        return new GenericDB(callback);

    if (typeof callback != 'function') {
        callback = function() {
        };
    }
    var gdb_object = this;
    // setup
    if (typeof GenericDB.options === 'undefined') {
        GenericDB.options = {
            driver_name: 'auto'
        };
    }

    this.limit_start = 0;
    this.limit_count = Infinity;

    if (GenericDB.options.hide) {
        callback(null);
        return;
    }

    options = GenericDB.options;
    // the provided driver
    var driver_name = options.driver_name || options.driver;
    if ('' === String(driver_name) && typeof GenericDB.driver !== 'undefined') {
        driver_name = GenericDB.driver;
    }
    // actual driver object
    var driver;
    switch (driver_name) {
        case 'indexed':
            driver = new IndexedDBDriver(options);
            break;
        case 'online':
            driver = new AjaxDriver(options);
            break;
        case 'sql':
            driver = new WebSQLDriver(options);
            break;
        default:
            // The driver may have been provided directly
            driver = options.custom_driver || options.driver;
    }


// the methods that have a direct equivalent
    var direct_callbacks = [
        'table', // Sets the working table or object store
        'count', // Get the number of records in a store with optional filtering
        'delete', // Delete from the store records that match the filter
        'getAll', // REturn all objects in the store, without filter testing
        'search', // Return all records that match the search
        'insert', // Insert a new record in the database
        'update' // Update the records that match the filter with the provided data
    ];
    // one loop that rules them all...
    for (var i = 0; i < direct_callbacks.length; i++) {
// the method name
        var method = direct_callbacks[i];
        // define the method
        this[method] = (function(local_method) {
            return function() {
                if (typeof driver[local_method] === 'function') {
//
// In PHP, this would be something like:
// return call_user_func_array(array($driver, $method), func_get_args())
//
                    return driver[local_method].apply(null, arguments);
                }
                ;
            };
        })(method);
    }

    GenericDB.RETURN_ARRAY = true;
    GenericDB.RETURN_OBJECT = !GenericDB.RETURN_ARRAY;
    // initialize the database driver
    driver.init(
            gdb_object
            , options.dbname || options.database
            , options.schema || options.tables
            , options.version || options.dbversion || options.db_version, function(raw_db) {
                if (typeof callback === 'function')
                    callback(gdb_object, raw_db);
            });
    GenericDB.translate_operations = function(filter) {
        var op_translation = {
            eq: '=',
            equal: '=',
            lt: '<',
            gt: '>',
            lte: '<=',
            gte: '>=',
            upperBound: '<=',
            upperBoundExclude: '<',
            lowerBound: '>=',
            lowerBoundExclude: '>',
            only: '=',
            regexp: 'regex',
            not: '!=',
            '<>': '!='
        };
        for (lettersOp in op_translation) {
            if (typeof filter[lettersOp] !== 'undefined') {
                filter[op_translation[lettersOp]] = filter[lettersOp];
            }
        }
    };
    GenericDB.isset = function(param) {
        return (typeof param !== 'undefined');
    };
    /**
     * Detects if the target record matches the filter
     * @param {object} filters
     * @param {object} record
     * @returns {boolean}
     */
    GenericDB.filterMatch = function(filters, record) {
        for (var field in filters) {
            if (!GenericDB.isset(record[field])) {
                return false;
            }

// regex comparition
            if (filters[field] instanceof RegExp) {
                if (null === String(record[field]).match(filters[field])) {
                    return false;
                }
            }

// direct comparition
            if (typeof filters[field] !== 'object') {
                if (String(record[field]).toLowerCase() !== String(filters[field]).toLowerCase()) {
                    return false;
                }
                continue;
            }

            GenericDB.translate_operations(filters[field]);
            // in search
            if (GenericDB.isset(filters[field]['in'])) {
                var found_in = false;
                for (var i = 0; i < filters[field]['in'].length; i++) {
                    if (String(filters[field]['in'][i]).toLowerCase() == String(record[field]).toLowerCase()) {
                        found_in = true;
                        break;
                    }
                }
                if (!found_in) {
                    return false;
                }
            }

// detect if any comparation fails
            if (GenericDB.isset(filters[field]['=']) && String(filters[field]['=']).toLowerCase() !== String(record[field]).toLowerCase()) {
                return false;
            }
            if (GenericDB.isset(filters[field]['!=']) && String(filters[field]['!=']).toLowerCase() == String(record[field]).toLowerCase()) {
                return false;
            }
            if (GenericDB.isset(filters[field]['<=']) && record[field] > filters[field]['<=']) {
                return false;
            }
            if (GenericDB.isset(filters[field]['<']) && record[field] >= filters[field]['<']) {
                return false;
            }
            if (GenericDB.isset(filters[field]['>=']) && record[field] < filters[field]['>=']) {
                return false;
            }
            if (GenericDB.isset(filters[field]['>']) && record[field] <= filters[field]['>']) {
                return false;
            }

// For SQL Like sintax
            if (GenericDB.isset(filters[field]['like'])) {
// source: http://phpjs.org/functions/preg_quote/
                var escaped = String(filters[field]['like']).replace(new RegExp('[.\\\\+*?\\[\\^\\]$(){}=!<>|:\\-]', 'g'), '\\$&');
                var regex = "";
                for (var chr = 0; chr < escaped.length; chr++) {
                    if (escaped[chr] === '%') {
                        if (escaped[chr + 1] !== '%') {
                            regex += '.*';
                            continue;
                        }
                    }

                    if (escaped[chr] === '_') {
                        if (escaped[chr - 1] !== '!') {
                            regex += '.';
                            continue;
                        }
                    }

                    regex += escaped[chr];
                }

                if (null === String(record[field]).match(new RegExp('^' + regex + '$', 'i'))) {
                    return false;
                }
            }

// for filters based on direct regex
            if (GenericDB.isset(filters[field]['regex'])) {
                var expr;
                if (filters[field]['regex'] instanceof RegExp) {
                    expr = filters[field]['regex'];
                } else {
                    expr = new RegExp(filters[field]['regex'], 'i');
                }

                if (null === String(record[field]).match(expr)) {
                    return false;
                }
            }

        }// foreach field in fields

        return true;
    };

    /**
     * Function to be called when an operation starts
     */
    if (!GenericDB.isset(GenericDB.beforeload)) {
        GenericDB.beforeload = function() {
        }
    }

    /**
     * Function to be called when an operation ends
     */
    if (!GenericDB.isset(GenericDB.afterload)) {
        GenericDB.afterload = function() {
        }
    }

    /**
     * Set the order fields.
     * In case the field for some particular object is undefined,
     * @returns {undefined}
     */
    this.order = function() {
        var args;
        if ((arguments[0]) instanceof Array){
            args = {};
            for (var i = 0; i < arguments[0].length; i++) {
                args[arguments[0][i]] = 'asc';
            }
        }else
        if (typeof arguments[0] !== 'object') {
            args = {};
            for (var i = 0; i < arguments.length; i++) {
                args[arguments[i]] = 'asc';
            }
        } else {
            args = arguments[0];
        }

        this.sort_order = args;
        return this;
    };

    GenericDB.sort = function(results, sort_by) {

        var work = [];// work copy
        var return_array;
        if (results instanceof Array) {
            return_array = true;
            for (var i = 0; i < results.length; i++) {
                work.push({
                    key: i,
                    value: results[i]
                });
            }
        } else {
            return_array = false;
            for (i in results) {
                work.push({
                    key: i,
                    value: results[i]
                });
            }
        }

        var quick = {
            sort: function(a, keys) {
                this.keys = keys;

                this.clone = a.slice(0);

                this.quicksort(0, this.clone.length - 1);

                return this.clone;
            },
            partition: function(left, right, pivotIndex) {
                var pivotValue = this.clone[pivotIndex];

                this.swap(pivotIndex, right);
                var storeIndex = left;
                for (var i = left; i < right; i++) {
                    if (this.compare(this.clone[i], pivotValue) < 1) {
                        this.swap(i, storeIndex);
                        storeIndex++;
                    }
                }

                this.swap(storeIndex, right);

                return storeIndex;
            },
            quicksort: function(left, right) {
                if (left < right) {
                    var pivotIndex = Math.floor(left + (right - left) / 2);
                    var pivotNewIndex = this.partition(left, right, pivotIndex);
                    this.quicksort(left, pivotNewIndex - 1);
                    this.quicksort(pivotNewIndex + 1, right);
                }
            },
            swap: function(i, j) {

                var temp = this.clone[i];
                this.clone[i] = this.clone[j];
                this.clone[j] = temp;
            },
            compare: function(a, b) {
                for (var field in this.keys) {
                    var direction = String(this.keys[field]).toLowerCase() == 'asc' ? -1 : 1;
                    if (!GenericDB.isset(a.value[field])) {
                        if (GenericDB.isset(b.value[field])) {
                            return direction;// NULL < b
                        } else {
                            continue;// null == null
                        }
                    } else {
                        if (!GenericDB.isset(b.value[field])) {
                            return -direction;// a > null
                        }
                    }

                    var work_a = typeof a.value[field] === 'string' ? a.value[field].toLowerCase() : a.value[field];
                    var work_b = typeof b.value[field] === 'string' ? b.value[field].toLowerCase() : b.value[field];

                    if (work_a < work_b) {
                        return direction;
                    }
                    if (work_a > work_b) {
                        return -direction;
                    }
                }

                return 0;
            }
        };

        var work = quick.sort(work, sort_by);

        var final;
        if (return_array) {
            final = [];
        } else {
            final = {};
        }

        for (var i = 0; i < work.length; i++) {
            if (return_array) {
                final[i] = work[i].value;
            } else {
                final[work[i].key] = work[i].value;
            }
        }

        return final;
    };

    this.sort_results = function(results) {
        return GenericDB.sort(results, this.sort_order);
    };

    this.limit = function(start, count) {
        if (!GenericDB.isset(start)) {
            count = Infinity;
            start = 0;
        }
        if (!GenericDB.isset(count)) {
            count = start;
            start = 0;
        }

        this.limit_start = start;
        this.limit_count = count;
        return gdb_object;
    };
}

/*********************************************************************************
 * ajax.js
 *********************************************************************************/

/**
 * AJAX-based GenericDB driver
 * Requires jQuery AJAX
 * @param {object} options setup options map
 * @returns {IndexedDBDriver} a new instance of IndexedDB driver
 */
function AjaxDriver(options) {
// Force this call to be treated as a constructor
    if (!(this instanceof AjaxDriver))
        return new AjaxDriver(options);
    
    this.base_url = options.base_url || '/';
    var this_object = this;

    var URL = {
        INIT: 'init',
        ALL: 'getAll',
        COUNT: 'count',
        DELETE: 'delete',
        INSERT: 'insert',
        SEARCH: 'search',
        UPDATE: 'update'
    };

    var current_store = null;

    this.runAJAX = function(url, data, callback) {
        var request = {};
        for (var key in data) {
            request[key] = JSON.stringify(data[key]);
        }
        
        request.table = JSON.stringify(request.table || current_store || '');
        
        jQuery.post((this_object.base_url) + '/dbapi/' + url,request).done(function(response) {
            if (typeof response != 'object') {
                response = JSON.parse(String(response));
            }
            callback(response.results, response.errors);
            //console.log(response.errors);
        }).fail(function (){
            callback(false, {
                fatal: 'failed to contact the server'
            });
        });
    };

    /**
     * Initialize the database based on the schema. For new versions, delete the database and repopulate with new schema
     * @param {string} dbname
     * @param {object} schema
     * @param {number} version
     * @param {function} callback will be called when the schema is ready
     * @returns {undefined}
     */
    this.init = function(parent, dbname, schema, version, callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }
        
        this_object.gdb_object = parent;

        if (typeof schema === 'undefined') {
            schema = {};
        }

        this_object.runAJAX(URL.INIT, {
            schema: schema
        }, function(response){
            callback(response);
        });


    }; // function IndexedDBDriver::init

    /**
     * Set the used object store
     * @param {string} store_name
     * @returns {undefined}
     */
    this.table = function(store_name) {

        current_store = store_name;

        return this_object.gdb_object;
    };
    /**
     * Get all records from the selected object store
     * @param {function} callback
     * @returns Array
     */
    this.getAll = function(callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();
        this_object.runAJAX(URL.ALL, {}, function(response){
            callback(this_object.gdb_object.sort_results(response));
            GenericDB.afterload();
        });

        return this_object.gdb_object;
    };

    /**
     * Insert a record in the object store
     * @param {object} record
     * @param {function} callback
     */
    this.insert = function(records, callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }

        if (!(records instanceof Array)) {
            records = [records];
        }

        GenericDB.beforeload();
        this_object.runAJAX(URL.INSERT, {
            records: records
        }, function(response, errors){
            if (response) {
                callback(true, response);
            } else {
                callback(false, errors);
            }
            GenericDB.afterload();
        });
    };
    this.count = function(filter, callback) {
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();

        if (typeof callback === 'undefined' && typeof filter === 'function') {
            callback = filter;
            filter = {};
        }

        this_object.runAJAX(URL.COUNT, {
            filter: filter
        }, function(response){
            callback(response);
            GenericDB.afterload();
        });

        return this_object.gdb_object;
    };
    /**
     * Perform a search query
     * @param {object} filters
     * @param {function} callback
     * @param {boolean} return_array
     * @returns {undefined}
     */
    this.search = function(filters, callback, return_array) {
        if (typeof return_array === 'undefined') {
            return_array = GenericDB.RETURN_ARRAY;
        } else {
            return_array = !!return_array; // ensure boolean
        }
        if (typeof callback != 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();

        var limit_start = this_object.gdb_object.limit_start;
        if (0 >= limit_start) {
            limit_start = undefined;
        }
        var limit_count = this_object.gdb_object.limit_count;
        if (Infinity == limit_count) {
            limit_count = undefined;
        }
        
        this_object.runAJAX(URL.SEARCH, {
            filter: filters,
            return_array: (return_array == GenericDB.RETURN_ARRAY),
            limit: limit_count,
            offset: limit_start
        }, function(response){
            callback(this_object.gdb_object.sort_results(response));
            GenericDB.afterload();
        });
        
        return this_object.gdb_object;
    };
    /**
     * Delete Assets that match the filter
     * @param {mixerd} filter the filter
     * @param {function} callback function(success, error)
     */
    this.delete = function(filter, callback) {

        if (typeof callback != 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();
        this_object.runAJAX(URL.DELETE, {
            filter: filter
        }, function(response){
            callback(response);
            GenericDB.afterload();
        });

        return this_object.gdb_object;
    };
    /**
     * Update records that match the filter with data provided
     * @param {mixed} filter
     * @param {object} data
     * @param {function} callback this callback will recieve the success state
     */
    this.update = function(filter, data, callback) {

        if (typeof callback != 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();
        this_object.runAJAX(URL.UPDATE, {
            filter: filter,
            data: data
        }, function(response,errors){
            callback(response, errors);
            GenericDB.afterload();
        });

        return this_object.gdb_object;
    };
}

/*********************************************************************************
 * indexed.js
 *********************************************************************************/

/**
 * Indexed DB GenericDB driver
 * @param {object} options setup options map
 * @returns {IndexedDBDriver} a new instance of IndexedDB driver
 */
function IndexedDBDriver(options) {
    // Force this call to be treated as a constructor
    if (!(this instanceof IndexedDBDriver))
        return new IndexedDBDriver(options);

    var INDEX_PREFIX = 'index_';

    var this_object = this;

    this.gdb_object = null;
    /**
     * @property String current_store The name of the object store to be used
     * @private
     */
    var current_store;
    /**
     * @property IDBEnvironment indexedDB Browser-specific IndexedDB API
     * @private
     */
    var indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || window.msIndexedDB;
    /**
     * @property {IDBTransaction} IDBTransaction The transaction class (Browser-specific)
     * @private
     */
    var IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction;
    /**
     * @property object schema the current database schema
     * @private
     */
    var db_schema = {};
    /**
     * Primary leys for each individual table/object store
     * @type object
     */
    var db_keys = {};
    /**
     * @property IDBDatabase db the database in use
     */
    var db;
    /**
     * Initialize the database based on the schema. For new versions, delete the database and repopulate with new schema
     * @param {GenericDB} parent
     * @param {string} dbname
     * @param {object} schema
     * @param {number} version
     * @param {function} callback
     * @returns {undefined}
     */
    this.init = function(parent, dbname, schema, version, callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }
        this_object.gdb_object = parent;

        if (typeof dbname === 'undefined') {
// if no name is provided, use the web host name
            dbname = window.location.host.replace(/[^a-z0-9]+/ig, '_');
        }

        if (typeof schema === 'undefined') {
            schema = {};
        }

        if (typeof version === 'undefined') {
// If no version is provided, use a day-specific number
            var d = new Date();
            //version = parseInt(d.getFullYear() + '' + d.getMonth() + '' + d.getDate());
            version = (d.getTime() - d.getTime() % 10000) / 10000;
        }

// save the schema
        db_schema = schema;
        // open the database
        var request = indexedDB.open(dbname, parseInt(version));
        //var request = indexedDB.open(dbname, 1);// debug

        var updateneeded = function(event) {
            var database = event.target.result;
            if (database instanceof IDBTransaction) {
                database = event.target.source;
            }
            for (table in schema) {
                var fields = schema[table];
                // remove the Object Store (can't alter the structure unless we do)
                if (database.objectStoreNames.contains(table))
                    database.deleteObjectStore(table);
                // get the primary key before creating the Object Store
                var pk;
                for (field_name in fields) {
                    if (fields[field_name].pk) {
                        pk = fields[field_name];
                        if (typeof pk.name === 'undefined') {
                            pk.name = field_name;
                        }
                        break;
                    }
                }

// create the Object Store (Equivalent to a table)
                var objectStore;
                if (pk) {
                    objectStore = database.createObjectStore(table, {
                        keyPath: INDEX_PREFIX + pk.name,
                        autoIncrement: pk.autoIncrement ? true : false
                    });
                    db_keys[table] = pk.name;
                } else {
                    objectStore = database.createObjectStore(table, {
                        autoIncrement: true
                    });
                    db_keys[table] = null;
                }

// -------------------------------------------------------------
// Mark the indexes
                for (var field_name in fields) {
                    var field_data = fields[field_name];
                    if (field_data.index || field_data.unique) {
                        objectStore.createIndex(
                                INDEX_PREFIX + field_name,
                                INDEX_PREFIX + field_name,
                                {unique: field_data.unique ? true : false}
                        );
                    }
// TODO See if it makes sense to support multi-entry indexes
                }

//TODO See if there is any need to do something with non-index fields (probably not)
            }// for each table in schema
        }; // function onupgradeneeded

        request.onupgradeneeded = updateneeded;
        request.onsuccess = function() {
            db = request.result;

            // Register the keys
            for (var table in schema) {
                var pk;
                for (var field_name in schema[table]) {
                    if (schema[table][field_name].pk) {
                        pk = schema[table][field_name];
                        if (typeof pk.name === 'undefined') {
                            pk.name = field_name;
                        }
                        break;
                    }
                }
                db_keys[table] = pk.name || '';
            }

            if (typeof db.setVersion === 'function') {
                if (db.version != version) {
                    var update_request = db.setVersion(version);
                    update_request.onsuccess = updateneeded;
                }
            }
            if (typeof callback === 'function')
                callback(db);
        };
        request.onerror = function(evt) {
            //console.log("IndexedDB error: " + (evt.target.errorCode || evt.target.webkitErrorMessage || evt.target.error));

        };
    }; // function IndexedDBDriver::init

    /**
     * Set the used object store
     * @param {string} store_name
     * @returns {undefined}
     */
    this.table = function(store_name) {

// wait for the database to be available
        while (!db)
            ;
        if (db && db.objectStoreNames && !db.objectStoreNames.contains(store_name)) {
            throw 'Invalid store selected';
        }
        current_store = store_name;

        this_object.gdb_object.limit_start = 0;
        this_object.gdb_object.limit_count = Infinity;
        this_object.gdb_object.sort_order = {};

        return this_object.gdb_object;
    };
    /**
     * Get all records from the selected object store
     * @param {function} callback
     * @returns Array
     */
    this.getAll = function(callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();
        var sort_order = this_object.gdb_object.sort_order;

        try {
            var objectStore = db.transaction(current_store)
                    .objectStore(current_store);
            var results = [];
            var request = objectStore.openCursor();
            request.onsuccess = function(e) {
                var cursor = e.target.result;
                if (cursor) {
                    results.push(this_object.decode_record(cursor.value));
                    cursor.continue();
                } else {
                    GenericDB.afterload();
                    this_object.gdb_object.sort_order = sort_order;
                    if (typeof callback === 'function')
                        callback(this_object.gdb_object.sort_results(results));
                }
            };
            request.onerror = function(e) {
                //console.log('Request error');
                GenericDB.afterload();
                callback('error', e);
            };
        } catch (e) {
            //console.log(e);
            GenericDB.afterload();
            callback([]);
        }
    };
    /**
     * Insert a record in the object store
     * @param {object} records
     * @param {function} callback
     */
    this.insert = function(records, callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();

// save the name of the store with this function call
        var cache_store = current_store;
        var insertpks = [];
        try {
            var transaction = db.transaction(cache_store, IDBTransaction.READ_WRITE || "readwrite");
            var store = transaction.objectStore(cache_store);
            if (!(records instanceof Array)) {
                records = [records];
            }

            var success = false;
            var key;
            for (var i = 0, len = records.length; i < len; i++) {
                var record = records[i];
                // populate the default values
                for (var colname in db_schema[cache_store]) {
                    if (typeof db_schema[cache_store][colname].default === 'undefined') {
                        continue;
                    }

                    if (typeof record[colname] !== 'undefined') {
                        continue;
                    }

                    record[colname] = db_schema[cache_store][colname].default;
                }

                var request = store.put(this_object.encode_record(record));
                request.onsuccess = function(e) {
                    success = true;
                    key = e.target.result;
                    insertpks.push(key);
                    //console.log('Inserted in ' + store.name + ' ' + e.target.result);
                };
                request.onerror = function(e) {
                    success = false;
                    //console.log(e);
                };
            }

            transaction.onabort = transaction.onerror = function(e) {
                GenericDB.afterload();
                if (typeof callback === 'function') {
                    callback(false, e);
                }
                //console.log('Insert Error in ' + store.name + ':' + e.message);
                //console.log(e);
            };
            transaction.oncomplete = function(e) {
                GenericDB.afterload();
                if (success) {
                    if (typeof callback === 'function') {
                        callback(true, insertpks);
                    }
                } else {
// call onerror handler
                    transaction.onerror(e);
                }
            };

        } catch (e) {
            //console.log(e);
        }
    };
    this.count = function(filter, callback) {
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();

        if (typeof callback !== 'function') {
            callback = filter;
            filter = {};
        }

        this_object.prepareRequest(filter, function(request, store, transaction) {
            var count = 0;
            request.onsuccess = function(e) {
                var cursor = e.target.result;
                if (cursor) {
                    count++;
                    cursor.continue();
                }
            };

            transaction.oncomplete = function(e) {
                GenericDB.afterload();
                callback(count);
            };

            request.onerror = transaction.onerror =
                    transaction.onabort = function(e) {
                        //console.log('Count error: ');
                        consolte.log(e);
                    };
        });

        return this_object.gdb_object;
    };
    /**
     * Create a Cursor request based on the filters and call the callback with the IDBRequest object and the transaction
     * @param {object} filters
     * @param {mixed} transaction_type
     * @param {function} callback function(IDBRequest request, IDBObjectStore store, IDBTransaction transaction)
     */
    this.prepareRequest = function(filters, transaction_type, callback) {
        if (typeof callback === 'undefined' && typeof transaction_type === 'function') {
            callback = transaction_type;
            transaction_type = typeof IDBTransaction.READ_ONLY !== 'undefined' ?
                    IDBTransaction.READ_ONLY :
                    'readonly';
        }

        var encoded_filter = this.encode_record(filters, false);

        var cache_store = current_store;
        // ---------------------------------------------------------------------
        // group filters based on their type
        var filter_groups = {
            pks: [], // Filters for Primary Keys
            unique: [], // Filters for unique indexes
            index: [], // Filters for regular indexes
            regular: []// Filters for regular fields
        };
        for (var field in filters) {
            var table = db_schema[cache_store];
            if (typeof table[field] === 'undefined') {
                filter_groups.regular.push({
                    key: field,
                    value: filters[field]
                });
            } else if (table[field].pk) {
                filter_groups.pks.push({
                    key: field,
                    value: encoded_filter['' + INDEX_PREFIX + field]
                });
            } else if (table[field].unique) {
                filter_groups.unique.push({
                    key: field,
                    value: encoded_filter['' + INDEX_PREFIX + field]
                });
            } else if (table[field].index) {
                filter_groups.index.push({
                    key: field,
                    value: encoded_filter['' + INDEX_PREFIX + field]
                });
            } else {
                filter_groups.regular.push({
                    key: field,
                    value: filters[field]
                });
            }
        }

// ---------------------------------------------------------------------
// prepare the request (Filter by the best criteria)
        var transaction = db.transaction(cache_store, transaction_type);
        var store = transaction.objectStore(cache_store);
        var request;
        var i;
        var getbounds = function(filter) {
            if (typeof filter !== 'object') {
                var stringified = String(filter);
                if (stringified.match(/[a-zA-Z]/) !== null) {
                    return IDBKeyRange.only(stringified.toLowerCase());
                }
                return IDBKeyRange.only(filter);
            }

// translate string operands
            GenericDB.translate_operations(filter);
            if (GenericDB.isset(filter['='])) {
                return IDBKeyRange.only(encode(filter['=']));
            }

            if (GenericDB.isset(filter['>='])) {
                if (GenericDB.isset(filter['<='])) {
// l <= pk <= u
                    return IDBKeyRange.bound(encode(filter['>=']), encode(filter['<=']));
                } else if (GenericDB.isset(filter['<'])) {
// l <= pk < u
                    return (IDBKeyRange.bound(encode(filter['>=']), encode(filter['<']), false, true));
                } else {
// l <= pk
                    return (IDBKeyRange.lowerBound(encode(filter['>='])));
                }
            } else if (GenericDB.isset(filter['>'])) {
                if (GenericDB.isset(filter['<='])) {
// l < pk <= u
                    return (IDBKeyRange.bound(encode(filter['>']), encode(filter['<=']), true));
                } else if (GenericDB.isset(filter['<'])) {
// l < pk < u
                    return (IDBKeyRange.bound(encode(filter['>']), encode(filter['<']), true, true));
                } else {
// l < pk
                    return (IDBKeyRange.lowerBound(encode(filter['>']), true));
                }
            } else if (GenericDB.isset(filter['<='])) {
// pk <= u
                return (IDBKeyRange.lowerBound(encode(filter['<='])));
            } else if (GenericDB.isset(filter['<'])) {
// pk < u
                return (IDBKeyRange.lowerBound(encode(filter['<']), true));
            }
        };

        for (i = 0; i < filter_groups.pks.length; i++) {
// pk may be provided as bounds
            var keyRange = getbounds(filter_groups.pks[i].value);
            if (keyRange) {
                request = store.openCursor(keyRange);
                break;
            }

        } // endfor (pks)

        if (!GenericDB.isset(request)) {
            var index_types = ['unique', 'index'];
            do {
                var group = index_types.shift();
                for (i = 0; i < filter_groups[group].length; i++) {
                    var keyRange = getbounds(filter_groups[group][i].value);
                    if (keyRange) {
                        var index = store.index(INDEX_PREFIX + filter_groups[group][i].key);
                        // index.get(filter_groups.index[0].value)
                        request = index.openCursor(keyRange);
                        break;
                    }
                }

                if (GenericDB.isset(request)) {
                    break;
                }

            } while (index_types.length > 0)
        }
// fallback -  when no bounds are provided, get all records (filtering is done manually)
        if (!GenericDB.isset(request)) {
            request = store.openCursor();
        }

        callback(request, store, transaction);
    };

    /**
     * Perform a search query
     * @param {object} filters
     * @param {function} callback
     * @param {boolean} return_array
     * @returns {undefined}
     */
    this.search = function(filters, callback, return_array) {
        if (typeof return_array === 'undefined') {
            return_array = GenericDB.RETURN_ARRAY;
        } else {
            return_array = !!return_array; // ensure boolean
        }
        if (typeof callback != 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();

        var limit_start = this_object.gdb_object.limit_start;
        var limit_count = this_object.gdb_object.limit_count;
        var sort_order = this_object.gdb_object.sort_order;

        this_object.prepareRequest(filters, 'readonly', function(request, store, transaction) {
            // ---------------------------------------------------------------------
            // Proccess the request and apply the filtering
            var records;
            var skiped = 0;
            if (return_array) {
                records = []; // return array
            } else {
                records = {}; // return object
            }
            request.onsuccess = function(event) {
                var cursor = event.target.result;
                if (!cursor) {
                    return;
                }
                var record = this_object.decode_record(cursor.value);
                if (GenericDB.filterMatch(filters, record)) {
                    if (skiped < limit_start) {
                        skiped++;
                    } else {
                        if (return_array === GenericDB.RETURN_ARRAY) {
                            records.push(record);
                        } else {
                            records[cursor.primaryKey] = record;
                        }
                    }
                }
                if (return_array === GenericDB.RETURN_ARRAY && records.length < limit_count) {
                    cursor.continue();
                } else if (return_array === GenericDB.RETURN_OBJECT && Object.keys(records).length < limit_count) {
                    cursor.continue();
                }
            };
            transaction.onabort = transaction.onerror =
                    request.onerror = function(e) {
                        GenericDB.afterload();
                        callback('error', e);
                    };
            transaction.oncomplete = function(e) {
                GenericDB.afterload();
                this_object.gdb_object.sort_order = sort_order;
                callback(this_object.gdb_object.sort_results(records));
            };
        });

        return this_object.gdb_object;
    };
    /**
     * Delete Assets that match the filter
     * @param {mixerd} filter the filter
     * @param {function} callback function(success, error)
     */
    this.delete = function(filter, callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();

        this_object.prepareRequest(filter, IDBTransaction.READ_WRITE || "readwrite", function(request, store, transaction) {

            var success = false;
            request.onsuccess = function(evn) {
                var cursor = evn.target.result;
                if (cursor) {
                    if (GenericDB.filterMatch(filter, this_object.decode_record(cursor.value))) {
                        cursor.delete();
                        //console.log('Deleting from ' + store.name + ' ' + cursor.primaryKey);
                    }
                    cursor.continue();
                } else {
                    success = true;
                }
            };
            transaction.onabort = transaction.onerror = request.onerror = function(e) {
                GenericDB.afterload();
                callback(false, e);
            };
            transaction.oncomplete = function(e) {
                GenericDB.afterload();
                callback(success, e);
            };
        });

        return this_object.gdb_object;
    };
    /**
     * Update records that match the filter with data provided
     * @param {mixed} filter
     * @param {object} data
     * @param {function} callback this callback will recieve the success state
     */
    this.update = function(filter, data, callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();

        data = this_object.encode_record(data);

        this_object.prepareRequest(filter, IDBTransaction.READ_WRITE || "readwrite", function(request, store, transaction) {

            var success = false;
            request.onsuccess = function(evn) {
                var cursor = evn.target.result;
                if (cursor) {
                    var target = cursor.value;
                    if (GenericDB.filterMatch(filter, this_object.decode_record(target))) {
                        for (var field in data) {
                            target[field] = data[field];
                        }
                        cursor.update(target);
                        //console.log('Updating from ' + store.name + ' ' + cursor.primaryKey);
                    }
                    cursor.continue();
                } else {
                    success = true;
                }
            };
            transaction.onabort = transaction.onerror = request.onerror = function(e) {
                GenericDB.afterload();
                callback(false, e);
            };
            transaction.oncomplete = function(e) {
                GenericDB.afterload();
                callback(success, e);
            };
        });

        return this_object.gdb_object;
    };

    /**
     * Transform a value to index-ready value.
     * We need this because IndexedDB is case-sensitive. 
     * This function returns the lower-cased value or a number if the string is numeric.
     * Numeric strings starting with 0, will be returned as string
     * @param {type} value
     * @returns {IndexedDBDriver.encode.encoded}
     */
    var encode = function(value) {
        var stringified = String(value);
        var encoded ;
        if (stringified.match(/[a-zA-Z]/)) {
            encoded = stringified.toLowerCase();
        } else if (stringified && !stringified.match(/[^0-9]/) && '0' != stringified[0]) {
            // only numbers found
            encoded = parseInt(value);
        } else {
            encoded = value;
        }
        
        return encoded;
    };

    /**
     * Transform raw record into IndexedDBDriver record (with "index_" prefix)
     * @param {object} original the input object
     * @param {boolean} keep_old
     * @returns {object}
     */
    this.encode_record = function(original, keep_old) {
        if ('undefined' == typeof keep_old) {
            keep_old = true;
        }

        var encoded = {};
        for (var field in original) {
            if (keep_old) {
                encoded[field] = original[field];
            }

            var value = db_schema[current_store][field];
            if (!GenericDB.isset(value)) {
                encoded[field] = original[field];
                continue;
            }

            if (value.index || value.unique || value.pk) {
                if ('object' == typeof original[field]) {
                    encoded[INDEX_PREFIX + field] = original[field];
                } else {
                    encoded[INDEX_PREFIX + field] = encode(original[field]);
                }
            } else {
                encoded[field] = original[field];
            }
        }

        if (!GenericDB.isset(encoded[db_keys[current_store]]) &&
                GenericDB.isset(encoded[INDEX_PREFIX + db_keys[current_store]])) {
            encoded[db_keys[current_store]] = encoded[INDEX_PREFIX + db_keys[current_store]];
        }

        return encoded;
    };
    /**
     * Transform IndexedDBDriver record (with "index_" prefix) into raw record
     * @param {object} original the input object
     * @returns {object}
     */
    this.decode_record = function(original) {
        var decoded = {};
        for (var field in original) {
            if (field.indexOf(INDEX_PREFIX) === 0) {
                var schema = db_schema[current_store][field.slice(6)];
                if (GenericDB.isset(schema) && (schema.index || schema.unique || schema.pk)) {
                    // skip indexes
                    continue;
                }
            }
            decoded[field] = original[field];
        }

        if (!GenericDB.isset(decoded[db_keys[current_store]]) && GenericDB.isset(original[INDEX_PREFIX + db_keys[current_store]])) {
            decoded[db_keys[current_store]] = original[INDEX_PREFIX + db_keys[current_store]];
        }

        return decoded;
    };
}

/*********************************************************************************
 * websql.js
 *********************************************************************************/


/**
 * Indexed DB GenericDB driver
 * @param {object} options setup options map
 * @returns {IndexedDBDriver} a new instance of IndexedDB driver
 */
function WebSQLDriver(options) {
// Force this call to be treated as a constructor
    if (!(this instanceof WebSQLDriver))
        return new WebSQLDriver(options);
    var this_object = this;

    this.gdb_object = null;
    /**
     * @property String current_store The name of the table to be used with the next data request
     * @private
     */
    var current_store;
    /**
     * @property object schema the current database schema
     * @private
     */
    var db_schema = {};
    var extra_column = 'extra-data';
    var extra_table = 'extra-data';
    /**
     * Primary leys for each individual table/object store
     * @type object
     */
    var db_keys = {};
    /**
     * @property IDBDatabase db the database in use
     */
    var db;
    var translate_type = function(raw_type) {
        switch (raw_type) {
            case 'string':
            case 'text':
                return 'text';
            case 'numeric':
            case 'int':
            case 'integer':
                return 'numeric';
        }

        return 'text';
    };

    var prepare_statement = function(query, data_in) {

        var data = data_in.slice(0);
        while (data.length > 0) {
            var data_field = data.shift();
            if (typeof data_field !== 'number') {
                data_field = String(data_field).replace(/"/g, '""');
                data_field = '"' + String(data_field).replace(/(\?)/g, '\\$1') + '"';
            }
            // replace the first question mark not escaped
            query = query.replace(/([^\\])\?/, '$1' + data_field);
        }

        return query.replace(/\\\?/g,'?');
    };

    /**
     * Initialize the database based on the schema. For new versions, delete the database and repopulate with new schema
     * @param {GenericDB} parent
     * @param {string} dbname
     * @param {object} schema
     * @param {number} version
     * @param {function} callback will be called when the schema is ready
     * @returns {undefined}
     */
    this.init = function(parent, dbname, schema, version, callback) {
        if (typeof callback !== 'function') {
            callback = function() {
            };
        }
        this_object.gdb_object = parent;

        if (typeof dbname === 'undefined') {
// if no name is provided, use the web host name
            dbname = window.location.host.replace(/[^a-z0-9]+/ig, '_');
        }

        if (typeof schema === 'undefined') {
            schema = {};
        }

        var dbsize;
        if (options && options.size) {
            dbsize = parseInt(options.size) || 134217728;
        } else {
            dbsize = 134217728; //134217728 = 128M
        }

        if (typeof version === 'undefined') {
// If no version is provided, use a day-specific number
            var d = new Date();
            //version = parseInt(d.getFullYear() + '' + d.getMonth() + '' + d.getDate());
            version = (d.getTime() - d.getTime() % 10000) / 10000;
        }

// save the schema
        db_schema = schema;
        // open the database
        var attempts = 0;
        do {
            try {
                db = openDatabase(dbname, '', dbname, dbsize);
                attempts = 10;
            } catch (e) {
                dbsize /= 2;
            }

            attempts++;
        } while (attempts < 6)

        if (String(version) !== String(db.version)) {
            db.changeVersion(String(db.version), String(version), function(tx) {
                for (table in schema) {
                    var fields = schema[table];
                    // remove the table first (for compatibility with IndexedDB implementation)
                    var delete_query = 'DROP TABLE IF EXISTS "' + table + '"';
                    //console.log(delete_query);
                    tx.executeSql(delete_query, [], function() {
                    }, function(tx, e) {
                        //console.log(e);
                    });
                    // get the primary key before creating the table

                    var indexes = [];
                    var column_definitions = [];
                    var pk_not_used = true;
                    for (field_name in fields) {
                        var field = fields[field_name];
                        var coltype = translate_type(field.type);
                        var definition = '"' + field_name + '" ';
                        if (field.pk) {
                            if (pk_not_used) {

                                if (field.autoIncrement) {
                                    definition += ' INTEGER PRIMARY KEY AUTOINCREMENT ';
                                } else {
                                    definition += ' ' + coltype + ' PRIMARY KEY ';
                                }

                                db_keys[table] = field_name;
                                pk_not_used = false;
                            } else {
                                definition += ' ' + coltype + ' UNIQUE ';
                            }
                        } else if (field.unique) {
                            definition += ' ' + coltype + ' UNIQUE ';
                        } else if (GenericDB.isset(field.default)) {
                            definition += ' ' + coltype + ' DEFAULT (\'' + String(field.default).replace("'", "\\'") + '\')';
                        } else {
                            definition += ' ' + coltype;
                        }

                        if (field.index || field.unique) {
                            indexes.push(field_name);
                        }

                        column_definitions.push(definition);
                    }

// Create a custom Primary Key if non is used
                    if (pk_not_used) {
                        db_keys[table] = 'gdb_auto_pk';
                        column_definitions.push('gdb_auto_pk INTEGER PRIMARY KEY AUTOINCREMENT');
                    }

                    // Add extra column
                    column_definitions.push('"' + extra_column + '" TEXT');

// create the table
                    var table_insert_query = 'CREATE TABLE "' + table + '" (' + column_definitions.join(',') + ')';
                    //console.log(table_insert_query);
                    tx.executeSql(table_insert_query, [], function(e) {
                    }, function(tx, e) {
                        //console.log(e);
                    });
                    /// Create the indexes
                    for (var i = 0; i < indexes.length; i++) {
                        var index_name = indexes[i];
                        var index_query = 'CREATE INDEX IF NOT EXISTS "' + table + '_' + index_name + '" ON "' + table + '" ("' + index_name + '")';
                        //console.log(index_query);
                        tx.executeSql(index_query, [], function() {
                        }, function(tx, e) {
                            //console.log(e);
                        });
                    }
                }// for each table in schema

// Create an extra table for extra columns data
                var extra_delete_query = 'DROP TABLE IF EXISTS "' + extra_table + '"';
                //console.log(extra_delete_query);
                tx.executeSql(extra_delete_query, [], function(e) {
                }, function(tx, e) {
                    //console.log(e);
                });
                var extra_query = 'CREATE TABLE "' + extra_table + '"(' +
                        'store TEXT,' +
                        'pk TEXT,' +
                        'param TEXT,' +
                        'value BLOB,' + // use Blob because it has no type
                        'PRIMARY KEY (store, pk, param) ' +
                        ')';
                //console.log(extra_query);
                tx.executeSql(extra_query, [], function() {
                }, function(tx, e) {
                    //console.log(e);
                });
                var pk_save = 'INSERT OR REPLACE INTO "' + extra_table + '" (store, pk, param, value) VALUES(\'\',\'\',\'pks\',?)';
                pk_save = prepare_statement(pk_save, [JSON.stringify(db_keys)]);
                //console.log(pk_save);
                tx.executeSql(pk_save, [], function() {
                }, function(tx, e) {
                    //console.log(e);
                });
            }, function(tx, error) {
                //console.log('WebSQL error ');
                //console.log(error);
            }, function() {
                callback(db);
            });
        } else {
            db.transaction(function(tx) {

                var pk_get = 'SELECT value FROM "' + extra_table + '" WHERE store = \'\' AND param=\'pks\' ';
                //console.log(pk_get);
                tx.executeSql(pk_get, [], function(tx, response) {
                    if (response.rows.length < 1) {
                        //console.log('Invalid Database. Please Empty the cache and continue');
                        return;
                    }

                    db_keys = JSON.parse(response.rows.item(0).value);
                    callback(db);
                }, function(tx, e) {
                    //console.log(e);
                });
            });
        }


    }; // function IndexedDBDriver::init

    /**
     * Set the used object store
     * @param {string} store_name
     * @returns {undefined}
     */
    this.table = function(store_name) {
        current_store = store_name;
        this_object.gdb_object.limit_start = 0;
        this_object.gdb_object.limit_count = Infinity;
        this_object.gdb_object.sort_order = {};

        return this_object.gdb_object;
    };
    /**
     * Get all records from the selected object store
     * @param {function} callback
     * @returns Array
     */
    this.getAll = function(callback) {
        if (typeof callback !== 'function') {
            callback = function() {
            };
        }
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();
        var sort_order = this_object.gdb_object.sort_order;

// save the current table name before it is changed
        var cache_table = current_store;
        try {

            db.transaction(function(tx) {
// get the core properties (from properites table)
                var core_data_query = 'SELECT * FROM "' + cache_table + '"';
                //console.log(core_data_query);
                tx.executeSql(core_data_query, [], function(tx, core_results) {
                    var results = []; //  a map (pk:value) with all the results

                    var core_len = core_results.rows.length;
                    for (var core_index = 0; core_index < core_len; core_index++) {
                        var value = core_results.rows.item(core_index);

                        try {
                            var extra_data = JSON.parse(value[extra_column]);
                            for (var data_key in extra_data) {
                                value[data_key] = extra_data[data_key];
                            }
                        } catch (e) {
                        }

                        results.push(value);
                    }

                    this_object.gdb_object.sort_order = sort_order;
                    callback(this_object.gdb_object.sort_results(results));

                }, function(tx, e) {
                    //console.log(e);
                });
            }, function(tx, error) {
                //console.log(error);
            });
        } catch (e) {
            //console.log(e);
            callback([]);
        }

        return this_object.gdb_object;
    };
    this.getOne = function(pk, callback, tx) {
        if (typeof callback !== 'function') {
            callback = function() {
            };
        }
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

// save the current table name before it is changed
        var cache_table = current_store;
        var pk_field = db_keys[cache_table];
        try {
            var transaction;
            if (GenericDB.isset(tx)) {
                transaction = function(transaction_callback) {
                    transaction_callback(tx);
                };
            } else {
                transaction = db.transaction;
            }

            transaction(function(tx) {
// get the core properties (from properites table)
                var core_data_query = 'SELECT * FROM "' + cache_table + '" WHERE "' + pk_field + '" = ?';
                core_data_query = prepare_statement(core_data_query, [pk]);
                //console.log(core_data_query);
                tx.executeSql(core_data_query, [], function(tx, core_results) {

                    var core_len = core_results.rows.length;
                    for (var core_index = 0; core_index < core_len; core_index++) {
                        var value = core_results.rows.item(core_index);

                        try {
                            var extra_data = JSON.parse(value[extra_column]);
                            for (var data_key in extra_data) {
                                value[data_key] = extra_data[data_key];
                            }
                        } catch (e) {
                        }

                        callback(value);
                        return;
                    }

                }, function(tx, e) {
                    //console.log(e);
                });
            }, function(tx, error) {
                //console.log(error);
            });
        } catch (e) {
            //console.log(e);
            callback([]);
        }

        return this_object.gdb_object;
    };
    /**
     * Insert a record in the object store
     * @param {object|array} records
     * @param {function} callback
     */
    this.insert = function(records, callback) {
        if (typeof callback !== 'function') {
            callback = function() {
            };
        }
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();

// save the name of the store with this function call
        var cache_table = current_store;
        var insertpks = [];
        try {

            db.transaction(function(tx) {
                if (!(records instanceof Array)) {
                    records = [records];
                }
                for (var i = 0, len = records.length; i < len; i++) {
                    (function() {
                        var record = records[i];
                        var columns = [];
                        var values = [];
                        var extra_columns = {};
                        // populate the default values
                        for (var colname in db_schema[cache_table]) {
                            if (typeof record[colname] === 'undefined') {
                                if (typeof db_schema[cache_table][colname].default !== 'undefined') {

                                    record[colname] = db_schema[cache_table][colname].default;
                                }
                            }
                        }

                        for (var colname in record) {
                            if (GenericDB.isset(db_schema[cache_table][colname])) {
                                columns.push(colname);
                                values.push(record[colname]);
                            } else {
                                extra_columns[colname] = record[colname];
                            }
                        }


                        var qm = []; // question marks
                        for (var qm_index = 0; qm_index < columns.length; qm_index++) {
                            qm.push('?');
                        }
                        
                        columns.push(extra_column);
                        values.push(JSON.stringify(extra_columns));
                        qm.push('?');

                        var insert_query = 'INSERT OR REPLACE INTO "' + cache_table +
                                '" ("' + columns.join('","') + '")' +
                                'VALUES (' + (qm.join(',')) + ')';
                        insert_query = prepare_statement(insert_query, values);
                        ////console.log(insert_query);

                        tx.executeSql(insert_query, [], function(tx, result) {
                            if (!GenericDB.isset(record[db_keys[cache_table]]))
                                record[db_keys[cache_table]] = result.insertId;
                            //console.log('Inserted in ' + cache_table + ' ' + record[db_keys[cache_table]]);
                            var pk = record[db_keys[cache_table]];

                            ////console.log('Done ' + insert_query);
                            insertpks.push(pk);

                        }, function(tx, e) {
                            //console.log('failed' + insert_query);
                            //console.log(e);
                        });
                    }());
                }
            }, function(tx, error) {
                //console.log(error);
            }, function() {
                GenericDB.afterload();
                if (typeof callback === 'function')
                    callback(true, insertpks);
            });
        } catch (e) {
            //console.log(e);
        }
    };

    /**
     * Get the number of records that match the filter
     * @param {object} filter
     * @param {function} callback
     * @returns {GenericDB}
     */
    this.count = function(filter, callback) {
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();

        if (typeof callback === 'undefined' && typeof filter === 'function') {
            callback = filter;
            filter = {};
        }

        this_object.search(filter, function(pks) {
            GenericDB.afterload();
            callback(pks.length || 0);
        });

        return this_object.gdb_object;
    };
    /**
     * Return a list of primary keys of records that match the search
     * @param {object} filters
     * @param {function} callback
     * @return {String} The full Where statement (including where keyword if necessary)
     */
    this.get_pks = function(filters, callback) {
        if (typeof callback !== 'function') {
            callback = function() {
            };
        }
        var cache_store = current_store;
        // ---------------------------------------------------------------------
        // group filters based on their type
        var index_filters = [];
        var regular_filters = [];
        for (var field in filters) {
            var table = db_schema[cache_store];
            if (typeof table[field] === 'undefined') {
                regular_filters.push({
                    key: field,
                    value: filters[field]
                });
                continue;
            }

            if (table[field].pk || table[field].unique || table[field].index) {
                index_filters.push({
                    key: field,
                    value: filters[field]
                });
            } else {
                regular_filters.push({
                    key: field,
                    value: filters[field]
                });
            }
        }

// ---------------------------------------------------------------------
// prepare the request (Filter by the best criteria)
        var pk_field = db_keys[cache_store];
        var getbounds = function(filter, numeric_key) {
            var ret_object = {
                query: [],
                data: []
            };

            if (typeof filter !== 'object') {
                if (numeric_key) {
                    ret_object.query[0] = '#column = ?';
                } else {
                    ret_object.query[0] = 'LOWER(#column) = LOWER(CAST( ? AS TEXT))';
                }
                ret_object.data[0] = filter;
                return ret_object;
            }

// translate string operands
            GenericDB.translate_operations(filter);
            if (GenericDB.isset(filter['='])) {
                if (numeric_key) {
                    ret_object.query[0] = '#column = ?';
                } else {
                    ret_object.query[0] = 'LOWER(#column) = LOWER(CAST( ? AS TEXT))';
                }
                ret_object.data[0] = filter['='];
                return ret_object;
            }

            if (GenericDB.isset(filter['>='])) {
                ret_object.query.push('#column >= ?');
                ret_object.data.push(filter['>=']);
            } else
            if (GenericDB.isset(filter['>'])) {
                ret_object.query.push('#column > ?');
                ret_object.data.push(filter['>']);
            }

            if (GenericDB.isset(filter['<='])) {
                ret_object.query.push('#column <= ?');
                ret_object.data.push(filter['<=']);
            } else
            if (GenericDB.isset(filter['<'])) {
                ret_object.query.push('#column < ?');
                ret_object.data.push(filter['<']);
            }

            if (GenericDB.isset(filter['like'])) {
                ret_object.query.push('#column LIKE ?');
                ret_object.data.push(filter['like']);
            }

            if (GenericDB.isset(filter['regex'])) {
                ret_object.query.push('#column REGEXP ?');
                ret_object.data.push(filter['regex']);
            }

            return ret_object;
        };
        var queries = [];
        var data = [];
        for (var i = 0; i < index_filters.length; i++) {
            var numeric_key = String(index_filters[i].value).match(/[^0-9]/) === null;
            var current = getbounds(index_filters[i].value, numeric_key);
            if (current.query.length < 1) {
                continue;
            }
            var query = current.query.join(' AND ');
            query = query.replace(/\#column/g, '"' + index_filters[i].key + '"');
            queries = queries.concat(query);
            data = data.concat(current.data);
        }

        var query_string = '';
        if (queries.length > 0) {
            query_string = ' WHERE ' + queries.join(' AND ');
        }

        if (typeof callback === 'function') {
            db.transaction(function(tx) {
                var final_query = 'SELECT "' + pk_field + '" FROM "' + cache_store + '" ' + query_string;
                final_query = prepare_statement(final_query, data);
                //console.log(final_query);
                tx.executeSql(final_query, [], function(tx, results) {
                    var pks = [], len = results.rows.length;
                    for (var i = 0; i < len; i++) {
                        pks.push(results.rows.item(i)[pk_field]);
                    }

                    if (len < 1) {
                        //console.log(final_query);
                    }

                    callback(pks, tx);
                }, function(tx, e) {
                    //console.log(e);
                });
            });
        }
        return  {
            query: query_string,
            data: data
        };
    };
    /**
     * Perform a search query
     * @param {object} filters
     * @param {function} callback
     * @param {boolean} return_array
     * @returns {undefined}
     */
    this.search = function(filters, callback, return_array) {
        if (typeof return_array === 'undefined') {
            return_array = GenericDB.RETURN_ARRAY;
        } else {
            return_array = !!return_array; // ensure boolean
        }
        if (typeof callback !== 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();
        var sort_order = this_object.gdb_object.sort_order;

        var cache_table = current_store;
        var pk_field = db_keys[cache_table];
        this_object.get_pks(filters, function(all_pks, tx) {

            if (all_pks.length < 1) {
                //console.log('Nothing found in ' + cache_table);
                //console.log(filters);
                GenericDB.afterload();
                callback([], tx);
                return;
            }
            
            var results;
            if (GenericDB.RETURN_ARRAY === return_array) {
                results = [];
            } else {
                results = {};
            }
            
            var finished_forks = 0;

            var slice_size = 10;
            var nr_forks = Math.ceil(all_pks.length / slice_size);
            var skiped = 0;
            var inserted = 0;
            for (var fork = 0; fork <= nr_forks; fork++) {
                (function() {
                    var pks = all_pks.slice(fork * slice_size, fork * slice_size + slice_size);
                    if (pks.length < 1)
                        return;
                    var qm_array = [];
                    for (var i = 0; i < pks.length; i++) {
                        qm_array.push('?');
                    }

                    var data_query = 'SELECT * FROM "' + cache_table + '" WHERE "' + pk_field + '" IN(' + qm_array.join(',') + ')';
                    data_query = prepare_statement(data_query, pks);
                    tx.executeSql(data_query, [], function(tx, data_results) {

                        for (var i = 0, len = data_results.rows.length; i < len; i++) {
                            if (inserted >= this_object.gdb_object.limit_count) {
                                break;
                            }
                            
                            var pk = data_results.rows.item(i)[pk_field];
                            var record = data_results.rows.item(i);

                            try {
                                var extra_data = JSON.parse(record[extra_column]);
                                for (var data_key in extra_data) {
                                    record[data_key] = extra_data[data_key];
                                }
                            } catch (e) {
                            }

                            if (GenericDB.filterMatch(filters, record)) {
                                if (skiped < this_object.gdb_object.limit_start) {
                                    skiped++;
                                } else {
                                    if (GenericDB.RETURN_ARRAY === return_array) {
                                        results.push(record);
                                    } else {
                                        results[pk] = record;
                                    }
                                    inserted++;
                                }
                            }
                            

                        }
                        finished_forks++;
                        
                        if (finished_forks >= nr_forks) {
                            this_object.gdb_object.sort_order = sort_order;
                            callback(this_object.gdb_object.sort_results(results), tx);
                        }

                    }, function(tx, e) {
                        //console.log(e);
                    });
                })();
            }

        });

        return this_object.gdb_object;
    };
    /**
     * Delete Assets that match the filter
     * @param {mixerd} filter the filter
     * @param {function} callback function(success, error)
     */
    this.delete = function(filter, callback) {

        if (typeof callback !== 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();

        var pk_field = db_keys[current_store];
        var cache_table = current_store;
        this_object.search(filter, function(results, tx) {
            if (results.length < 1) {
                GenericDB.afterload();
                callback(true);
                return;
            }
            var pks = [];
            var qm = [];
            for (var i = 0, len = results.length; i < len; i++) {
                pks.push(results[i][pk_field]);
                qm.push('?');
            }

            var delete_query = 'DELETE FROM "' + cache_table + '" WHERE "' + pk_field + '" IN(' + qm.join(',') + ')';
            delete_query = prepare_statement(delete_query, pks);
            //console.log(delete_query);
            tx.executeSql(delete_query, [], function() {
                GenericDB.afterload();
                callback(true);
            }, function(tx, e) {
                //console.log(e);
                GenericDB.afterload();
                callback(false);
            });
        });

        return this_object.gdb_object;
    };
    /**
     * Update records that match the filter with data provided
     * @param {mixed} filter
     * @param {object} data
     * @param {function} callback this callback will recieve the success state
     */
    this.update = function(filter, data, callback) {

        if (typeof callback !== 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();

        var pk_field = db_keys[current_store];
        var cache_table = current_store;
        var indexed_data = [];
        var extra_data = {};
        var indexed_qm = [];
        // filter the data into indexed (in the table) and non-indexed (in extra table)
        for (var field in data) {
            var schema = db_schema[cache_table][field];
            if (typeof schema === 'undefined') {
                 extra_data[field] = data[field];
                continue;
            }

            if (schema.pk || schema.unique || schema.index) {
                indexed_data.push({
                    key: field,
                    value: data[field]
                });
                indexed_qm.push('?');
            } else {
                extra_data[field] = data[field];
            }
        }

        this_object.search(filter, function(results, tx) {
            var sql_data = [];
            var prepared_statements = [];
            for (var i = 0; i < indexed_data.length; i++) {
                prepared_statements.push('"' + indexed_data[i].key + '" = ?');
                sql_data.push(indexed_data[i].value);
            }

            prepared_statements.push('"' + extra_column + '" = ?');
            sql_data.push(JSON.stringify(extra_data));

            var qm = [];
            var pks = [];
            for (var i = 0, len = results.length; i < len; i++) {
                qm.push('?');
                pks.push(results[i][pk_field]);
            }

            var update_query = 'UPDATE "' + cache_table + '" SET ' + prepared_statements.join(',')
                    + ' WHERE "' + db_keys[cache_table] + '" IN(' + qm.join(',') + ')';
            //console.log(update_query);
            update_query = prepare_statement(update_query, sql_data.concat(pks));
            tx.executeSql(update_query, [], function(tx, e) {
                //console.log(e);
                GenericDB.afterload();
                callback(true);

            });
        });

        return this_object.gdb_object;
    };
}
