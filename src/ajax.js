/**
 * AJAX-based GenericDB driver
 * Requires jQuery AJAX
 * @param {object} options setup options map
 * @returns {IndexedDBDriver} a new instance of IndexedDB driver
 */
function AjaxDriver(options) {
// Force this call to be treated as a constructor
    if (!(this instanceof AjaxDriver))
        return new AjaxDriver(options);
    
    this.base_url = options.base_url || '/';
    var this_object = this;

    var URL = {
        INIT: 'init',
        ALL: 'getAll',
        COUNT: 'count',
        DELETE: 'delete',
        INSERT: 'insert',
        SEARCH: 'search',
        UPDATE: 'update'
    };

    var current_store = null;

    this.runAJAX = function(url, data, callback) {
        var request = {};
        for (var key in data) {
            request[key] = JSON.stringify(data[key]);
        }
        
        request.table = JSON.stringify(request.table || current_store || '');
        
        jQuery.post((this_object.base_url) + '/dbapi/' + url,request).done(function(response) {
            if (typeof response != 'object') {
                response = JSON.parse(String(response));
            }
            callback(response.results, response.errors);
            //console.log(response.errors);
        }).fail(function (){
            callback(false, {
                fatal: 'failed to contact the server'
            });
        });
    };

    /**
     * Initialize the database based on the schema. For new versions, delete the database and repopulate with new schema
     * @param {string} dbname
     * @param {object} schema
     * @param {number} version
     * @param {function} callback will be called when the schema is ready
     * @returns {undefined}
     */
    this.init = function(parent, dbname, schema, version, callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }
        
        this_object.gdb_object = parent;

        if (typeof schema === 'undefined') {
            schema = {};
        }

        this_object.runAJAX(URL.INIT, {
            schema: schema
        }, function(response){
            callback(response);
        });


    }; // function IndexedDBDriver::init

    /**
     * Set the used object store
     * @param {string} store_name
     * @returns {undefined}
     */
    this.table = function(store_name) {

        current_store = store_name;

        return this_object.gdb_object;
    };
    /**
     * Get all records from the selected object store
     * @param {function} callback
     * @returns Array
     */
    this.getAll = function(callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();
        this_object.runAJAX(URL.ALL, {}, function(response){
            callback(this_object.gdb_object.sort_results(response));
            GenericDB.afterload();
        });

        return this_object.gdb_object;
    };

    /**
     * Insert a record in the object store
     * @param {object} record
     * @param {function} callback
     */
    this.insert = function(records, callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }

        if (!(records instanceof Array)) {
            records = [records];
        }

        GenericDB.beforeload();
        this_object.runAJAX(URL.INSERT, {
            records: records
        }, function(response, errors){
            if (response) {
                callback(true, response);
            } else {
                callback(false, errors);
            }
            GenericDB.afterload();
        });
    };
    this.count = function(filter, callback) {
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();

        if (typeof callback === 'undefined' && typeof filter === 'function') {
            callback = filter;
            filter = {};
        }

        this_object.runAJAX(URL.COUNT, {
            filter: filter
        }, function(response){
            callback(response);
            GenericDB.afterload();
        });

        return this_object.gdb_object;
    };
    /**
     * Perform a search query
     * @param {object} filters
     * @param {function} callback
     * @param {boolean} return_array
     * @returns {undefined}
     */
    this.search = function(filters, callback, return_array) {
        if (typeof return_array === 'undefined') {
            return_array = GenericDB.RETURN_ARRAY;
        } else {
            return_array = !!return_array; // ensure boolean
        }
        if (typeof callback != 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();

        var limit_start = this_object.gdb_object.limit_start;
        if (0 >= limit_start) {
            limit_start = undefined;
        }
        var limit_count = this_object.gdb_object.limit_count;
        if (Infinity == limit_count) {
            limit_count = undefined;
        }
        
        this_object.runAJAX(URL.SEARCH, {
            filter: filters,
            return_array: (return_array == GenericDB.RETURN_ARRAY),
            limit: limit_count,
            offset: limit_start
        }, function(response){
            callback(this_object.gdb_object.sort_results(response));
            GenericDB.afterload();
        });
        
        return this_object.gdb_object;
    };
    /**
     * Delete Assets that match the filter
     * @param {mixerd} filter the filter
     * @param {function} callback function(success, error)
     */
    this.delete = function(filter, callback) {

        if (typeof callback != 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();
        this_object.runAJAX(URL.DELETE, {
            filter: filter
        }, function(response){
            callback(response);
            GenericDB.afterload();
        });

        return this_object.gdb_object;
    };
    /**
     * Update records that match the filter with data provided
     * @param {mixed} filter
     * @param {object} data
     * @param {function} callback this callback will recieve the success state
     */
    this.update = function(filter, data, callback) {

        if (typeof callback != 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();
        this_object.runAJAX(URL.UPDATE, {
            filter: filter,
            data: data
        }, function(response,errors){
            callback(response, errors);
            GenericDB.afterload();
        });

        return this_object.gdb_object;
    };
}
