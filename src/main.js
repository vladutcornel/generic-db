/**
 * A generic database
 * @param {function} callback The function called when the database is ready
 * @returns {GenericDB}
 */
function GenericDB(callback) {
// Force this call to be treated as a constructor
    if (!(this instanceof GenericDB))
        return new GenericDB(callback);

    if (typeof callback != 'function') {
        callback = function() {
        };
    }
    var gdb_object = this;
    // setup
    if (typeof GenericDB.options === 'undefined') {
        GenericDB.options = {
            driver_name: 'auto'
        };
    }

    this.limit_start = 0;
    this.limit_count = Infinity;

    if (GenericDB.options.hide) {
        callback(null);
        return;
    }

    options = GenericDB.options;
    // the provided driver
    var driver_name = options.driver_name || options.driver;
    if ('' === String(driver_name) && typeof GenericDB.driver !== 'undefined') {
        driver_name = GenericDB.driver;
    }
    // actual driver object
    var driver;
    switch (driver_name) {
        case 'indexed':
            driver = new IndexedDBDriver(options);
            break;
        case 'online':
            driver = new AjaxDriver(options);
            break;
        case 'sql':
            driver = new WebSQLDriver(options);
            break;
        default:
            // The driver may have been provided directly
            driver = options.custom_driver || options.driver;
    }


// the methods that have a direct equivalent
    var direct_callbacks = [
        'table', // Sets the working table or object store
        'count', // Get the number of records in a store with optional filtering
        'delete', // Delete from the store records that match the filter
        'getAll', // REturn all objects in the store, without filter testing
        'search', // Return all records that match the search
        'insert', // Insert a new record in the database
        'update' // Update the records that match the filter with the provided data
    ];
    // one loop that rules them all...
    for (var i = 0; i < direct_callbacks.length; i++) {
// the method name
        var method = direct_callbacks[i];
        // define the method
        this[method] = (function(local_method) {
            return function() {
                if (typeof driver[local_method] === 'function') {
//
// In PHP, this would be something like:
// return call_user_func_array(array($driver, $method), func_get_args())
//
                    return driver[local_method].apply(null, arguments);
                }
                ;
            };
        })(method);
    }

    GenericDB.RETURN_ARRAY = true;
    GenericDB.RETURN_OBJECT = !GenericDB.RETURN_ARRAY;
    // initialize the database driver
    driver.init(
            gdb_object
            , options.dbname || options.database
            , options.schema || options.tables
            , options.version || options.dbversion || options.db_version, function(raw_db) {
                if (typeof callback === 'function')
                    callback(gdb_object, raw_db);
            });
    GenericDB.translate_operations = function(filter) {
        var op_translation = {
            eq: '=',
            equal: '=',
            lt: '<',
            gt: '>',
            lte: '<=',
            gte: '>=',
            upperBound: '<=',
            upperBoundExclude: '<',
            lowerBound: '>=',
            lowerBoundExclude: '>',
            only: '=',
            regexp: 'regex',
            not: '!=',
            '<>': '!='
        };
        for (lettersOp in op_translation) {
            if (typeof filter[lettersOp] !== 'undefined') {
                filter[op_translation[lettersOp]] = filter[lettersOp];
            }
        }
    };
    GenericDB.isset = function(param) {
        return (typeof param !== 'undefined');
    };
    /**
     * Detects if the target record matches the filter
     * @param {object} filters
     * @param {object} record
     * @returns {boolean}
     */
    GenericDB.filterMatch = function(filters, record) {
        for (var field in filters) {
            if (!GenericDB.isset(record[field])) {
                return false;
            }

// regex comparition
            if (filters[field] instanceof RegExp) {
                if (null === String(record[field]).match(filters[field])) {
                    return false;
                }
            }

// direct comparition
            if (typeof filters[field] !== 'object') {
                if (String(record[field]).toLowerCase() !== String(filters[field]).toLowerCase()) {
                    return false;
                }
                continue;
            }

            GenericDB.translate_operations(filters[field]);
            // in search
            if (GenericDB.isset(filters[field]['in'])) {
                var found_in = false;
                for (var i = 0; i < filters[field]['in'].length; i++) {
                    if (String(filters[field]['in'][i]).toLowerCase() == String(record[field]).toLowerCase()) {
                        found_in = true;
                        break;
                    }
                }
                if (!found_in) {
                    return false;
                }
            }

// detect if any comparation fails
            if (GenericDB.isset(filters[field]['=']) && String(filters[field]['=']).toLowerCase() !== String(record[field]).toLowerCase()) {
                return false;
            }
            if (GenericDB.isset(filters[field]['!=']) && String(filters[field]['!=']).toLowerCase() == String(record[field]).toLowerCase()) {
                return false;
            }
            if (GenericDB.isset(filters[field]['<=']) && record[field] > filters[field]['<=']) {
                return false;
            }
            if (GenericDB.isset(filters[field]['<']) && record[field] >= filters[field]['<']) {
                return false;
            }
            if (GenericDB.isset(filters[field]['>=']) && record[field] < filters[field]['>=']) {
                return false;
            }
            if (GenericDB.isset(filters[field]['>']) && record[field] <= filters[field]['>']) {
                return false;
            }

// For SQL Like sintax
            if (GenericDB.isset(filters[field]['like'])) {
// source: http://phpjs.org/functions/preg_quote/
                var escaped = String(filters[field]['like']).replace(new RegExp('[.\\\\+*?\\[\\^\\]$(){}=!<>|:\\-]', 'g'), '\\$&');
                var regex = "";
                for (var chr = 0; chr < escaped.length; chr++) {
                    if (escaped[chr] === '%') {
                        if (escaped[chr + 1] !== '%') {
                            regex += '.*';
                            continue;
                        }
                    }

                    if (escaped[chr] === '_') {
                        if (escaped[chr - 1] !== '!') {
                            regex += '.';
                            continue;
                        }
                    }

                    regex += escaped[chr];
                }

                if (null === String(record[field]).match(new RegExp('^' + regex + '$', 'i'))) {
                    return false;
                }
            }

// for filters based on direct regex
            if (GenericDB.isset(filters[field]['regex'])) {
                var expr;
                if (filters[field]['regex'] instanceof RegExp) {
                    expr = filters[field]['regex'];
                } else {
                    expr = new RegExp(filters[field]['regex'], 'i');
                }

                if (null === String(record[field]).match(expr)) {
                    return false;
                }
            }

        }// foreach field in fields

        return true;
    };

    /**
     * Function to be called when an operation starts
     */
    if (!GenericDB.isset(GenericDB.beforeload)) {
        GenericDB.beforeload = function() {
        }
    }

    /**
     * Function to be called when an operation ends
     */
    if (!GenericDB.isset(GenericDB.afterload)) {
        GenericDB.afterload = function() {
        }
    }

    /**
     * Set the order fields.
     * In case the field for some particular object is undefined,
     * @returns {undefined}
     */
    this.order = function() {
        var args;
        if ((arguments[0]) instanceof Array){
            args = {};
            for (var i = 0; i < arguments[0].length; i++) {
                args[arguments[0][i]] = 'asc';
            }
        }else
        if (typeof arguments[0] !== 'object') {
            args = {};
            for (var i = 0; i < arguments.length; i++) {
                args[arguments[i]] = 'asc';
            }
        } else {
            args = arguments[0];
        }

        this.sort_order = args;
        return this;
    };

    GenericDB.sort = function(results, sort_by) {

        var work = [];// work copy
        var return_array;
        if (results instanceof Array) {
            return_array = true;
            for (var i = 0; i < results.length; i++) {
                work.push({
                    key: i,
                    value: results[i]
                });
            }
        } else {
            return_array = false;
            for (i in results) {
                work.push({
                    key: i,
                    value: results[i]
                });
            }
        }

        var quick = {
            sort: function(a, keys) {
                this.keys = keys;

                this.clone = a.slice(0);

                this.quicksort(0, this.clone.length - 1);

                return this.clone;
            },
            partition: function(left, right, pivotIndex) {
                var pivotValue = this.clone[pivotIndex];

                this.swap(pivotIndex, right);
                var storeIndex = left;
                for (var i = left; i < right; i++) {
                    if (this.compare(this.clone[i], pivotValue) < 1) {
                        this.swap(i, storeIndex);
                        storeIndex++;
                    }
                }

                this.swap(storeIndex, right);

                return storeIndex;
            },
            quicksort: function(left, right) {
                if (left < right) {
                    var pivotIndex = Math.floor(left + (right - left) / 2);
                    var pivotNewIndex = this.partition(left, right, pivotIndex);
                    this.quicksort(left, pivotNewIndex - 1);
                    this.quicksort(pivotNewIndex + 1, right);
                }
            },
            swap: function(i, j) {

                var temp = this.clone[i];
                this.clone[i] = this.clone[j];
                this.clone[j] = temp;
            },
            compare: function(a, b) {
                for (var field in this.keys) {
                    var direction = String(this.keys[field]).toLowerCase() == 'asc' ? -1 : 1;
                    if (!GenericDB.isset(a.value[field])) {
                        if (GenericDB.isset(b.value[field])) {
                            return direction;// NULL < b
                        } else {
                            continue;// null == null
                        }
                    } else {
                        if (!GenericDB.isset(b.value[field])) {
                            return -direction;// a > null
                        }
                    }

                    var work_a = typeof a.value[field] === 'string' ? a.value[field].toLowerCase() : a.value[field];
                    var work_b = typeof b.value[field] === 'string' ? b.value[field].toLowerCase() : b.value[field];

                    if (work_a < work_b) {
                        return direction;
                    }
                    if (work_a > work_b) {
                        return -direction;
                    }
                }

                return 0;
            }
        };

        var work = quick.sort(work, sort_by);

        var final;
        if (return_array) {
            final = [];
        } else {
            final = {};
        }

        for (var i = 0; i < work.length; i++) {
            if (return_array) {
                final[i] = work[i].value;
            } else {
                final[work[i].key] = work[i].value;
            }
        }

        return final;
    };

    this.sort_results = function(results) {
        return GenericDB.sort(results, this.sort_order);
    };

    this.limit = function(start, count) {
        if (!GenericDB.isset(start)) {
            count = Infinity;
            start = 0;
        }
        if (!GenericDB.isset(count)) {
            count = start;
            start = 0;
        }

        this.limit_start = start;
        this.limit_count = count;
        return gdb_object;
    };
}
