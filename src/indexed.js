/**
 * Indexed DB GenericDB driver
 * @param {object} options setup options map
 * @returns {IndexedDBDriver} a new instance of IndexedDB driver
 */
function IndexedDBDriver(options) {
    // Force this call to be treated as a constructor
    if (!(this instanceof IndexedDBDriver))
        return new IndexedDBDriver(options);

    var INDEX_PREFIX = 'index_';

    var this_object = this;

    this.gdb_object = null;
    /**
     * @property String current_store The name of the object store to be used
     * @private
     */
    var current_store;
    /**
     * @property IDBEnvironment indexedDB Browser-specific IndexedDB API
     * @private
     */
    var indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || window.msIndexedDB;
    /**
     * @property {IDBTransaction} IDBTransaction The transaction class (Browser-specific)
     * @private
     */
    var IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction;
    /**
     * @property object schema the current database schema
     * @private
     */
    var db_schema = {};
    /**
     * Primary leys for each individual table/object store
     * @type object
     */
    var db_keys = {};
    /**
     * @property IDBDatabase db the database in use
     */
    var db;
    /**
     * Initialize the database based on the schema. For new versions, delete the database and repopulate with new schema
     * @param {GenericDB} parent
     * @param {string} dbname
     * @param {object} schema
     * @param {number} version
     * @param {function} callback
     * @returns {undefined}
     */
    this.init = function(parent, dbname, schema, version, callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }
        this_object.gdb_object = parent;

        if (typeof dbname === 'undefined') {
// if no name is provided, use the web host name
            dbname = window.location.host.replace(/[^a-z0-9]+/ig, '_');
        }

        if (typeof schema === 'undefined') {
            schema = {};
        }

        if (typeof version === 'undefined') {
// If no version is provided, use a day-specific number
            var d = new Date();
            //version = parseInt(d.getFullYear() + '' + d.getMonth() + '' + d.getDate());
            version = (d.getTime() - d.getTime() % 10000) / 10000;
        }

// save the schema
        db_schema = schema;
        // open the database
        var request = indexedDB.open(dbname, parseInt(version));
        //var request = indexedDB.open(dbname, 1);// debug

        var updateneeded = function(event) {
            var database = event.target.result;
            if (database instanceof IDBTransaction) {
                database = event.target.source;
            }
            for (table in schema) {
                var fields = schema[table];
                // remove the Object Store (can't alter the structure unless we do)
                if (database.objectStoreNames.contains(table))
                    database.deleteObjectStore(table);
                // get the primary key before creating the Object Store
                var pk;
                for (field_name in fields) {
                    if (fields[field_name].pk) {
                        pk = fields[field_name];
                        if (typeof pk.name === 'undefined') {
                            pk.name = field_name;
                        }
                        break;
                    }
                }

// create the Object Store (Equivalent to a table)
                var objectStore;
                if (pk) {
                    objectStore = database.createObjectStore(table, {
                        keyPath: INDEX_PREFIX + pk.name,
                        autoIncrement: pk.autoIncrement ? true : false
                    });
                    db_keys[table] = pk.name;
                } else {
                    objectStore = database.createObjectStore(table, {
                        autoIncrement: true
                    });
                    db_keys[table] = null;
                }

// -------------------------------------------------------------
// Mark the indexes
                for (var field_name in fields) {
                    var field_data = fields[field_name];
                    if (field_data.index || field_data.unique) {
                        objectStore.createIndex(
                                INDEX_PREFIX + field_name,
                                INDEX_PREFIX + field_name,
                                {unique: field_data.unique ? true : false}
                        );
                    }
// TODO See if it makes sense to support multi-entry indexes
                }

//TODO See if there is any need to do something with non-index fields (probably not)
            }// for each table in schema
        }; // function onupgradeneeded

        request.onupgradeneeded = updateneeded;
        request.onsuccess = function() {
            db = request.result;

            // Register the keys
            for (var table in schema) {
                var pk;
                for (var field_name in schema[table]) {
                    if (schema[table][field_name].pk) {
                        pk = schema[table][field_name];
                        if (typeof pk.name === 'undefined') {
                            pk.name = field_name;
                        }
                        break;
                    }
                }
                db_keys[table] = pk.name || '';
            }

            if (typeof db.setVersion === 'function') {
                if (db.version != version) {
                    var update_request = db.setVersion(version);
                    update_request.onsuccess = updateneeded;
                }
            }
            if (typeof callback === 'function')
                callback(db);
        };
        request.onerror = function(evt) {
            //console.log("IndexedDB error: " + (evt.target.errorCode || evt.target.webkitErrorMessage || evt.target.error));

        };
    }; // function IndexedDBDriver::init

    /**
     * Set the used object store
     * @param {string} store_name
     * @returns {undefined}
     */
    this.table = function(store_name) {

// wait for the database to be available
        while (!db)
            ;
        if (db && db.objectStoreNames && !db.objectStoreNames.contains(store_name)) {
            throw 'Invalid store selected';
        }
        current_store = store_name;

        this_object.gdb_object.limit_start = 0;
        this_object.gdb_object.limit_count = Infinity;
        this_object.gdb_object.sort_order = {};

        return this_object.gdb_object;
    };
    /**
     * Get all records from the selected object store
     * @param {function} callback
     * @returns Array
     */
    this.getAll = function(callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();
        var sort_order = this_object.gdb_object.sort_order;

        try {
            var objectStore = db.transaction(current_store)
                    .objectStore(current_store);
            var results = [];
            var request = objectStore.openCursor();
            request.onsuccess = function(e) {
                var cursor = e.target.result;
                if (cursor) {
                    results.push(this_object.decode_record(cursor.value));
                    cursor.continue();
                } else {
                    GenericDB.afterload();
                    this_object.gdb_object.sort_order = sort_order;
                    if (typeof callback === 'function')
                        callback(this_object.gdb_object.sort_results(results));
                }
            };
            request.onerror = function(e) {
                //console.log('Request error');
                GenericDB.afterload();
                callback('error', e);
            };
        } catch (e) {
            //console.log(e);
            GenericDB.afterload();
            callback([]);
        }
    };
    /**
     * Insert a record in the object store
     * @param {object} records
     * @param {function} callback
     */
    this.insert = function(records, callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();

// save the name of the store with this function call
        var cache_store = current_store;
        var insertpks = [];
        try {
            var transaction = db.transaction(cache_store, IDBTransaction.READ_WRITE || "readwrite");
            var store = transaction.objectStore(cache_store);
            if (!(records instanceof Array)) {
                records = [records];
            }

            var success = false;
            var key;
            for (var i = 0, len = records.length; i < len; i++) {
                var record = records[i];
                // populate the default values
                for (var colname in db_schema[cache_store]) {
                    if (typeof db_schema[cache_store][colname].default === 'undefined') {
                        continue;
                    }

                    if (typeof record[colname] !== 'undefined') {
                        continue;
                    }

                    record[colname] = db_schema[cache_store][colname].default;
                }

                var request = store.put(this_object.encode_record(record));
                request.onsuccess = function(e) {
                    success = true;
                    key = e.target.result;
                    insertpks.push(key);
                    //console.log('Inserted in ' + store.name + ' ' + e.target.result);
                };
                request.onerror = function(e) {
                    success = false;
                    //console.log(e);
                };
            }

            transaction.onabort = transaction.onerror = function(e) {
                GenericDB.afterload();
                if (typeof callback === 'function') {
                    callback(false, e);
                }
                //console.log('Insert Error in ' + store.name + ':' + e.message);
                //console.log(e);
            };
            transaction.oncomplete = function(e) {
                GenericDB.afterload();
                if (success) {
                    if (typeof callback === 'function') {
                        callback(true, insertpks);
                    }
                } else {
// call onerror handler
                    transaction.onerror(e);
                }
            };

        } catch (e) {
            //console.log(e);
        }
    };
    this.count = function(filter, callback) {
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();

        if (typeof callback !== 'function') {
            callback = filter;
            filter = {};
        }

        this_object.prepareRequest(filter, function(request, store, transaction) {
            var count = 0;
            request.onsuccess = function(e) {
                var cursor = e.target.result;
                if (cursor) {
                    count++;
                    cursor.continue();
                }
            };

            transaction.oncomplete = function(e) {
                GenericDB.afterload();
                callback(count);
            };

            request.onerror = transaction.onerror =
                    transaction.onabort = function(e) {
                        //console.log('Count error: ');
                        consolte.log(e);
                    };
        });

        return this_object.gdb_object;
    };
    /**
     * Create a Cursor request based on the filters and call the callback with the IDBRequest object and the transaction
     * @param {object} filters
     * @param {mixed} transaction_type
     * @param {function} callback function(IDBRequest request, IDBObjectStore store, IDBTransaction transaction)
     */
    this.prepareRequest = function(filters, transaction_type, callback) {
        if (typeof callback === 'undefined' && typeof transaction_type === 'function') {
            callback = transaction_type;
            transaction_type = typeof IDBTransaction.READ_ONLY !== 'undefined' ?
                    IDBTransaction.READ_ONLY :
                    'readonly';
        }

        var encoded_filter = this.encode_record(filters, false);

        var cache_store = current_store;
        // ---------------------------------------------------------------------
        // group filters based on their type
        var filter_groups = {
            pks: [], // Filters for Primary Keys
            unique: [], // Filters for unique indexes
            index: [], // Filters for regular indexes
            regular: []// Filters for regular fields
        };
        for (var field in filters) {
            var table = db_schema[cache_store];
            if (typeof table[field] === 'undefined') {
                filter_groups.regular.push({
                    key: field,
                    value: filters[field]
                });
            } else if (table[field].pk) {
                filter_groups.pks.push({
                    key: field,
                    value: encoded_filter['' + INDEX_PREFIX + field]
                });
            } else if (table[field].unique) {
                filter_groups.unique.push({
                    key: field,
                    value: encoded_filter['' + INDEX_PREFIX + field]
                });
            } else if (table[field].index) {
                filter_groups.index.push({
                    key: field,
                    value: encoded_filter['' + INDEX_PREFIX + field]
                });
            } else {
                filter_groups.regular.push({
                    key: field,
                    value: filters[field]
                });
            }
        }

// ---------------------------------------------------------------------
// prepare the request (Filter by the best criteria)
        var transaction = db.transaction(cache_store, transaction_type);
        var store = transaction.objectStore(cache_store);
        var request;
        var i;
        var getbounds = function(filter) {
            if (typeof filter !== 'object') {
                var stringified = String(filter);
                if (stringified.match(/[a-zA-Z]/) !== null) {
                    return IDBKeyRange.only(stringified.toLowerCase());
                }
                return IDBKeyRange.only(filter);
            }

// translate string operands
            GenericDB.translate_operations(filter);
            if (GenericDB.isset(filter['='])) {
                return IDBKeyRange.only(encode(filter['=']));
            }

            if (GenericDB.isset(filter['>='])) {
                if (GenericDB.isset(filter['<='])) {
// l <= pk <= u
                    return IDBKeyRange.bound(encode(filter['>=']), encode(filter['<=']));
                } else if (GenericDB.isset(filter['<'])) {
// l <= pk < u
                    return (IDBKeyRange.bound(encode(filter['>=']), encode(filter['<']), false, true));
                } else {
// l <= pk
                    return (IDBKeyRange.lowerBound(encode(filter['>='])));
                }
            } else if (GenericDB.isset(filter['>'])) {
                if (GenericDB.isset(filter['<='])) {
// l < pk <= u
                    return (IDBKeyRange.bound(encode(filter['>']), encode(filter['<=']), true));
                } else if (GenericDB.isset(filter['<'])) {
// l < pk < u
                    return (IDBKeyRange.bound(encode(filter['>']), encode(filter['<']), true, true));
                } else {
// l < pk
                    return (IDBKeyRange.lowerBound(encode(filter['>']), true));
                }
            } else if (GenericDB.isset(filter['<='])) {
// pk <= u
                return (IDBKeyRange.lowerBound(encode(filter['<='])));
            } else if (GenericDB.isset(filter['<'])) {
// pk < u
                return (IDBKeyRange.lowerBound(encode(filter['<']), true));
            }
        };

        for (i = 0; i < filter_groups.pks.length; i++) {
// pk may be provided as bounds
            var keyRange = getbounds(filter_groups.pks[i].value);
            if (keyRange) {
                request = store.openCursor(keyRange);
                break;
            }

        } // endfor (pks)

        if (!GenericDB.isset(request)) {
            var index_types = ['unique', 'index'];
            do {
                var group = index_types.shift();
                for (i = 0; i < filter_groups[group].length; i++) {
                    var keyRange = getbounds(filter_groups[group][i].value);
                    if (keyRange) {
                        var index = store.index(INDEX_PREFIX + filter_groups[group][i].key);
                        // index.get(filter_groups.index[0].value)
                        request = index.openCursor(keyRange);
                        break;
                    }
                }

                if (GenericDB.isset(request)) {
                    break;
                }

            } while (index_types.length > 0)
        }
// fallback -  when no bounds are provided, get all records (filtering is done manually)
        if (!GenericDB.isset(request)) {
            request = store.openCursor();
        }

        callback(request, store, transaction);
    };

    /**
     * Perform a search query
     * @param {object} filters
     * @param {function} callback
     * @param {boolean} return_array
     * @returns {undefined}
     */
    this.search = function(filters, callback, return_array) {
        if (typeof return_array === 'undefined') {
            return_array = GenericDB.RETURN_ARRAY;
        } else {
            return_array = !!return_array; // ensure boolean
        }
        if (typeof callback != 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();

        var limit_start = this_object.gdb_object.limit_start;
        var limit_count = this_object.gdb_object.limit_count;
        var sort_order = this_object.gdb_object.sort_order;

        this_object.prepareRequest(filters, 'readonly', function(request, store, transaction) {
            // ---------------------------------------------------------------------
            // Proccess the request and apply the filtering
            var records;
            var skiped = 0;
            if (return_array) {
                records = []; // return array
            } else {
                records = {}; // return object
            }
            request.onsuccess = function(event) {
                var cursor = event.target.result;
                if (!cursor) {
                    return;
                }
                var record = this_object.decode_record(cursor.value);
                if (GenericDB.filterMatch(filters, record)) {
                    if (skiped < limit_start) {
                        skiped++;
                    } else {
                        if (return_array === GenericDB.RETURN_ARRAY) {
                            records.push(record);
                        } else {
                            records[cursor.primaryKey] = record;
                        }
                    }
                }
                if (return_array === GenericDB.RETURN_ARRAY && records.length < limit_count) {
                    cursor.continue();
                } else if (return_array === GenericDB.RETURN_OBJECT && Object.keys(records).length < limit_count) {
                    cursor.continue();
                }
            };
            transaction.onabort = transaction.onerror =
                    request.onerror = function(e) {
                        GenericDB.afterload();
                        callback('error', e);
                    };
            transaction.oncomplete = function(e) {
                GenericDB.afterload();
                this_object.gdb_object.sort_order = sort_order;
                callback(this_object.gdb_object.sort_results(records));
            };
        });

        return this_object.gdb_object;
    };
    /**
     * Delete Assets that match the filter
     * @param {mixerd} filter the filter
     * @param {function} callback function(success, error)
     */
    this.delete = function(filter, callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();

        this_object.prepareRequest(filter, IDBTransaction.READ_WRITE || "readwrite", function(request, store, transaction) {

            var success = false;
            request.onsuccess = function(evn) {
                var cursor = evn.target.result;
                if (cursor) {
                    if (GenericDB.filterMatch(filter, this_object.decode_record(cursor.value))) {
                        cursor.delete();
                        //console.log('Deleting from ' + store.name + ' ' + cursor.primaryKey);
                    }
                    cursor.continue();
                } else {
                    success = true;
                }
            };
            transaction.onabort = transaction.onerror = request.onerror = function(e) {
                GenericDB.afterload();
                callback(false, e);
            };
            transaction.oncomplete = function(e) {
                GenericDB.afterload();
                callback(success, e);
            };
        });

        return this_object.gdb_object;
    };
    /**
     * Update records that match the filter with data provided
     * @param {mixed} filter
     * @param {object} data
     * @param {function} callback this callback will recieve the success state
     */
    this.update = function(filter, data, callback) {
        if (typeof callback != 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();

        data = this_object.encode_record(data);

        this_object.prepareRequest(filter, IDBTransaction.READ_WRITE || "readwrite", function(request, store, transaction) {

            var success = false;
            request.onsuccess = function(evn) {
                var cursor = evn.target.result;
                if (cursor) {
                    var target = cursor.value;
                    if (GenericDB.filterMatch(filter, this_object.decode_record(target))) {
                        for (var field in data) {
                            target[field] = data[field];
                        }
                        cursor.update(target);
                        //console.log('Updating from ' + store.name + ' ' + cursor.primaryKey);
                    }
                    cursor.continue();
                } else {
                    success = true;
                }
            };
            transaction.onabort = transaction.onerror = request.onerror = function(e) {
                GenericDB.afterload();
                callback(false, e);
            };
            transaction.oncomplete = function(e) {
                GenericDB.afterload();
                callback(success, e);
            };
        });

        return this_object.gdb_object;
    };

    /**
     * Transform a value to index-ready value.
     * We need this because IndexedDB is case-sensitive. 
     * This function returns the lower-cased value or a number if the string is numeric.
     * Numeric strings starting with 0, will be returned as string
     * @param {type} value
     * @returns {IndexedDBDriver.encode.encoded}
     */
    var encode = function(value) {
        var stringified = String(value);
        var encoded ;
        if (stringified.match(/[a-zA-Z]/)) {
            encoded = stringified.toLowerCase();
        } else if (stringified && !stringified.match(/[^0-9]/) && '0' != stringified[0]) {
            // only numbers found
            encoded = parseInt(value);
        } else {
            encoded = value;
        }
        
        return encoded;
    };

    /**
     * Transform raw record into IndexedDBDriver record (with "index_" prefix)
     * @param {object} original the input object
     * @param {boolean} keep_old
     * @returns {object}
     */
    this.encode_record = function(original, keep_old) {
        if ('undefined' == typeof keep_old) {
            keep_old = true;
        }

        var encoded = {};
        for (var field in original) {
            if (keep_old) {
                encoded[field] = original[field];
            }

            var value = db_schema[current_store][field];
            if (!GenericDB.isset(value)) {
                encoded[field] = original[field];
                continue;
            }

            if (value.index || value.unique || value.pk) {
                if ('object' == typeof original[field]) {
                    encoded[INDEX_PREFIX + field] = original[field];
                } else {
                    encoded[INDEX_PREFIX + field] = encode(original[field]);
                }
            } else {
                encoded[field] = original[field];
            }
        }

        if (!GenericDB.isset(encoded[db_keys[current_store]]) &&
                GenericDB.isset(encoded[INDEX_PREFIX + db_keys[current_store]])) {
            encoded[db_keys[current_store]] = encoded[INDEX_PREFIX + db_keys[current_store]];
        }

        return encoded;
    };
    /**
     * Transform IndexedDBDriver record (with "index_" prefix) into raw record
     * @param {object} original the input object
     * @returns {object}
     */
    this.decode_record = function(original) {
        var decoded = {};
        for (var field in original) {
            if (field.indexOf(INDEX_PREFIX) === 0) {
                var schema = db_schema[current_store][field.slice(6)];
                if (GenericDB.isset(schema) && (schema.index || schema.unique || schema.pk)) {
                    // skip indexes
                    continue;
                }
            }
            decoded[field] = original[field];
        }

        if (!GenericDB.isset(decoded[db_keys[current_store]]) && GenericDB.isset(original[INDEX_PREFIX + db_keys[current_store]])) {
            decoded[db_keys[current_store]] = original[INDEX_PREFIX + db_keys[current_store]];
        }

        return decoded;
    };
}
