
/**
 * Indexed DB GenericDB driver
 * @param {object} options setup options map
 * @returns {IndexedDBDriver} a new instance of IndexedDB driver
 */
function WebSQLDriver(options) {
// Force this call to be treated as a constructor
    if (!(this instanceof WebSQLDriver))
        return new WebSQLDriver(options);
    var this_object = this;

    this.gdb_object = null;
    /**
     * @property String current_store The name of the table to be used with the next data request
     * @private
     */
    var current_store;
    /**
     * @property object schema the current database schema
     * @private
     */
    var db_schema = {};
    var extra_column = 'extra-data';
    var extra_table = 'extra-data';
    /**
     * Primary leys for each individual table/object store
     * @type object
     */
    var db_keys = {};
    /**
     * @property IDBDatabase db the database in use
     */
    var db;
    var translate_type = function(raw_type) {
        switch (raw_type) {
            case 'string':
            case 'text':
                return 'text';
            case 'numeric':
            case 'int':
            case 'integer':
                return 'numeric';
        }

        return 'text';
    };

    var prepare_statement = function(query, data_in) {

        var data = data_in.slice(0);
        while (data.length > 0) {
            var data_field = data.shift();
            if (typeof data_field !== 'number') {
                data_field = String(data_field).replace(/"/g, '""');
                data_field = '"' + String(data_field).replace(/(\?)/g, '\\$1') + '"';
            }
            // replace the first question mark not escaped
            query = query.replace(/([^\\])\?/, '$1' + data_field);
        }

        return query.replace(/\\\?/g,'?');
    };

    /**
     * Initialize the database based on the schema. For new versions, delete the database and repopulate with new schema
     * @param {GenericDB} parent
     * @param {string} dbname
     * @param {object} schema
     * @param {number} version
     * @param {function} callback will be called when the schema is ready
     * @returns {undefined}
     */
    this.init = function(parent, dbname, schema, version, callback) {
        if (typeof callback !== 'function') {
            callback = function() {
            };
        }
        this_object.gdb_object = parent;

        if (typeof dbname === 'undefined') {
// if no name is provided, use the web host name
            dbname = window.location.host.replace(/[^a-z0-9]+/ig, '_');
        }

        if (typeof schema === 'undefined') {
            schema = {};
        }

        var dbsize;
        if (options && options.size) {
            dbsize = parseInt(options.size) || 134217728;
        } else {
            dbsize = 134217728; //134217728 = 128M
        }

        if (typeof version === 'undefined') {
// If no version is provided, use a day-specific number
            var d = new Date();
            //version = parseInt(d.getFullYear() + '' + d.getMonth() + '' + d.getDate());
            version = (d.getTime() - d.getTime() % 10000) / 10000;
        }

// save the schema
        db_schema = schema;
        // open the database
        var attempts = 0;
        do {
            try {
                db = openDatabase(dbname, '', dbname, dbsize);
                attempts = 10;
            } catch (e) {
                dbsize /= 2;
            }

            attempts++;
        } while (attempts < 6)

        if (String(version) !== String(db.version)) {
            db.changeVersion(String(db.version), String(version), function(tx) {
                for (table in schema) {
                    var fields = schema[table];
                    // remove the table first (for compatibility with IndexedDB implementation)
                    var delete_query = 'DROP TABLE IF EXISTS "' + table + '"';
                    //console.log(delete_query);
                    tx.executeSql(delete_query, [], function() {
                    }, function(tx, e) {
                        //console.log(e);
                    });
                    // get the primary key before creating the table

                    var indexes = [];
                    var column_definitions = [];
                    var pk_not_used = true;
                    for (field_name in fields) {
                        var field = fields[field_name];
                        var coltype = translate_type(field.type);
                        var definition = '"' + field_name + '" ';
                        if (field.pk) {
                            if (pk_not_used) {

                                if (field.autoIncrement) {
                                    definition += ' INTEGER PRIMARY KEY AUTOINCREMENT ';
                                } else {
                                    definition += ' ' + coltype + ' PRIMARY KEY ';
                                }

                                db_keys[table] = field_name;
                                pk_not_used = false;
                            } else {
                                definition += ' ' + coltype + ' UNIQUE ';
                            }
                        } else if (field.unique) {
                            definition += ' ' + coltype + ' UNIQUE ';
                        } else if (GenericDB.isset(field.default)) {
                            definition += ' ' + coltype + ' DEFAULT (\'' + String(field.default).replace("'", "\\'") + '\')';
                        } else {
                            definition += ' ' + coltype;
                        }

                        if (field.index || field.unique) {
                            indexes.push(field_name);
                        }

                        column_definitions.push(definition);
                    }

// Create a custom Primary Key if non is used
                    if (pk_not_used) {
                        db_keys[table] = 'gdb_auto_pk';
                        column_definitions.push('gdb_auto_pk INTEGER PRIMARY KEY AUTOINCREMENT');
                    }

                    // Add extra column
                    column_definitions.push('"' + extra_column + '" TEXT');

// create the table
                    var table_insert_query = 'CREATE TABLE "' + table + '" (' + column_definitions.join(',') + ')';
                    //console.log(table_insert_query);
                    tx.executeSql(table_insert_query, [], function(e) {
                    }, function(tx, e) {
                        //console.log(e);
                    });
                    /// Create the indexes
                    for (var i = 0; i < indexes.length; i++) {
                        var index_name = indexes[i];
                        var index_query = 'CREATE INDEX IF NOT EXISTS "' + table + '_' + index_name + '" ON "' + table + '" ("' + index_name + '")';
                        //console.log(index_query);
                        tx.executeSql(index_query, [], function() {
                        }, function(tx, e) {
                            //console.log(e);
                        });
                    }
                }// for each table in schema

// Create an extra table for extra columns data
                var extra_delete_query = 'DROP TABLE IF EXISTS "' + extra_table + '"';
                //console.log(extra_delete_query);
                tx.executeSql(extra_delete_query, [], function(e) {
                }, function(tx, e) {
                    //console.log(e);
                });
                var extra_query = 'CREATE TABLE "' + extra_table + '"(' +
                        'store TEXT,' +
                        'pk TEXT,' +
                        'param TEXT,' +
                        'value BLOB,' + // use Blob because it has no type
                        'PRIMARY KEY (store, pk, param) ' +
                        ')';
                //console.log(extra_query);
                tx.executeSql(extra_query, [], function() {
                }, function(tx, e) {
                    //console.log(e);
                });
                var pk_save = 'INSERT OR REPLACE INTO "' + extra_table + '" (store, pk, param, value) VALUES(\'\',\'\',\'pks\',?)';
                pk_save = prepare_statement(pk_save, [JSON.stringify(db_keys)]);
                //console.log(pk_save);
                tx.executeSql(pk_save, [], function() {
                }, function(tx, e) {
                    //console.log(e);
                });
            }, function(tx, error) {
                //console.log('WebSQL error ');
                //console.log(error);
            }, function() {
                callback(db);
            });
        } else {
            db.transaction(function(tx) {

                var pk_get = 'SELECT value FROM "' + extra_table + '" WHERE store = \'\' AND param=\'pks\' ';
                //console.log(pk_get);
                tx.executeSql(pk_get, [], function(tx, response) {
                    if (response.rows.length < 1) {
                        //console.log('Invalid Database. Please Empty the cache and continue');
                        return;
                    }

                    db_keys = JSON.parse(response.rows.item(0).value);
                    callback(db);
                }, function(tx, e) {
                    //console.log(e);
                });
            });
        }


    }; // function IndexedDBDriver::init

    /**
     * Set the used object store
     * @param {string} store_name
     * @returns {undefined}
     */
    this.table = function(store_name) {
        current_store = store_name;
        this_object.gdb_object.limit_start = 0;
        this_object.gdb_object.limit_count = Infinity;
        this_object.gdb_object.sort_order = {};

        return this_object.gdb_object;
    };
    /**
     * Get all records from the selected object store
     * @param {function} callback
     * @returns Array
     */
    this.getAll = function(callback) {
        if (typeof callback !== 'function') {
            callback = function() {
            };
        }
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();
        var sort_order = this_object.gdb_object.sort_order;

// save the current table name before it is changed
        var cache_table = current_store;
        try {

            db.transaction(function(tx) {
// get the core properties (from properites table)
                var core_data_query = 'SELECT * FROM "' + cache_table + '"';
                //console.log(core_data_query);
                tx.executeSql(core_data_query, [], function(tx, core_results) {
                    var results = []; //  a map (pk:value) with all the results

                    var core_len = core_results.rows.length;
                    for (var core_index = 0; core_index < core_len; core_index++) {
                        var value = core_results.rows.item(core_index);

                        try {
                            var extra_data = JSON.parse(value[extra_column]);
                            for (var data_key in extra_data) {
                                value[data_key] = extra_data[data_key];
                            }
                        } catch (e) {
                        }

                        results.push(value);
                    }

                    this_object.gdb_object.sort_order = sort_order;
                    callback(this_object.gdb_object.sort_results(results));

                }, function(tx, e) {
                    //console.log(e);
                });
            }, function(tx, error) {
                //console.log(error);
            });
        } catch (e) {
            //console.log(e);
            callback([]);
        }

        return this_object.gdb_object;
    };
    this.getOne = function(pk, callback, tx) {
        if (typeof callback !== 'function') {
            callback = function() {
            };
        }
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

// save the current table name before it is changed
        var cache_table = current_store;
        var pk_field = db_keys[cache_table];
        try {
            var transaction;
            if (GenericDB.isset(tx)) {
                transaction = function(transaction_callback) {
                    transaction_callback(tx);
                };
            } else {
                transaction = db.transaction;
            }

            transaction(function(tx) {
// get the core properties (from properites table)
                var core_data_query = 'SELECT * FROM "' + cache_table + '" WHERE "' + pk_field + '" = ?';
                core_data_query = prepare_statement(core_data_query, [pk]);
                //console.log(core_data_query);
                tx.executeSql(core_data_query, [], function(tx, core_results) {

                    var core_len = core_results.rows.length;
                    for (var core_index = 0; core_index < core_len; core_index++) {
                        var value = core_results.rows.item(core_index);

                        try {
                            var extra_data = JSON.parse(value[extra_column]);
                            for (var data_key in extra_data) {
                                value[data_key] = extra_data[data_key];
                            }
                        } catch (e) {
                        }

                        callback(value);
                        return;
                    }

                }, function(tx, e) {
                    //console.log(e);
                });
            }, function(tx, error) {
                //console.log(error);
            });
        } catch (e) {
            //console.log(e);
            callback([]);
        }

        return this_object.gdb_object;
    };
    /**
     * Insert a record in the object store
     * @param {object|array} records
     * @param {function} callback
     */
    this.insert = function(records, callback) {
        if (typeof callback !== 'function') {
            callback = function() {
            };
        }
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();

// save the name of the store with this function call
        var cache_table = current_store;
        var insertpks = [];
        try {

            db.transaction(function(tx) {
                if (!(records instanceof Array)) {
                    records = [records];
                }
                for (var i = 0, len = records.length; i < len; i++) {
                    (function() {
                        var record = records[i];
                        var columns = [];
                        var values = [];
                        var extra_columns = {};
                        // populate the default values
                        for (var colname in db_schema[cache_table]) {
                            if (typeof record[colname] === 'undefined') {
                                if (typeof db_schema[cache_table][colname].default !== 'undefined') {

                                    record[colname] = db_schema[cache_table][colname].default;
                                }
                            }
                        }

                        for (var colname in record) {
                            if (GenericDB.isset(db_schema[cache_table][colname])) {
                                columns.push(colname);
                                values.push(record[colname]);
                            } else {
                                extra_columns[colname] = record[colname];
                            }
                        }


                        var qm = []; // question marks
                        for (var qm_index = 0; qm_index < columns.length; qm_index++) {
                            qm.push('?');
                        }
                        
                        columns.push(extra_column);
                        values.push(JSON.stringify(extra_columns));
                        qm.push('?');

                        var insert_query = 'INSERT OR REPLACE INTO "' + cache_table +
                                '" ("' + columns.join('","') + '")' +
                                'VALUES (' + (qm.join(',')) + ')';
                        insert_query = prepare_statement(insert_query, values);
                        ////console.log(insert_query);

                        tx.executeSql(insert_query, [], function(tx, result) {
                            if (!GenericDB.isset(record[db_keys[cache_table]]))
                                record[db_keys[cache_table]] = result.insertId;
                            //console.log('Inserted in ' + cache_table + ' ' + record[db_keys[cache_table]]);
                            var pk = record[db_keys[cache_table]];

                            ////console.log('Done ' + insert_query);
                            insertpks.push(pk);

                        }, function(tx, e) {
                            //console.log('failed' + insert_query);
                            //console.log(e);
                        });
                    }());
                }
            }, function(tx, error) {
                //console.log(error);
            }, function() {
                GenericDB.afterload();
                if (typeof callback === 'function')
                    callback(true, insertpks);
            });
        } catch (e) {
            //console.log(e);
        }
    };

    /**
     * Get the number of records that match the filter
     * @param {object} filter
     * @param {function} callback
     * @returns {GenericDB}
     */
    this.count = function(filter, callback) {
        if (!current_store) {
            throw "The Store wasn't set. Please use DB.table() method before this"
        }

        GenericDB.beforeload();

        if (typeof callback === 'undefined' && typeof filter === 'function') {
            callback = filter;
            filter = {};
        }

        this_object.search(filter, function(pks) {
            GenericDB.afterload();
            callback(pks.length || 0);
        });

        return this_object.gdb_object;
    };
    /**
     * Return a list of primary keys of records that match the search
     * @param {object} filters
     * @param {function} callback
     * @return {String} The full Where statement (including where keyword if necessary)
     */
    this.get_pks = function(filters, callback) {
        if (typeof callback !== 'function') {
            callback = function() {
            };
        }
        var cache_store = current_store;
        // ---------------------------------------------------------------------
        // group filters based on their type
        var index_filters = [];
        var regular_filters = [];
        for (var field in filters) {
            var table = db_schema[cache_store];
            if (typeof table[field] === 'undefined') {
                regular_filters.push({
                    key: field,
                    value: filters[field]
                });
                continue;
            }

            if (table[field].pk || table[field].unique || table[field].index) {
                index_filters.push({
                    key: field,
                    value: filters[field]
                });
            } else {
                regular_filters.push({
                    key: field,
                    value: filters[field]
                });
            }
        }

// ---------------------------------------------------------------------
// prepare the request (Filter by the best criteria)
        var pk_field = db_keys[cache_store];
        var getbounds = function(filter, numeric_key) {
            var ret_object = {
                query: [],
                data: []
            };

            if (typeof filter !== 'object') {
                if (numeric_key) {
                    ret_object.query[0] = '#column = ?';
                } else {
                    ret_object.query[0] = 'LOWER(#column) = LOWER(CAST( ? AS TEXT))';
                }
                ret_object.data[0] = filter;
                return ret_object;
            }

// translate string operands
            GenericDB.translate_operations(filter);
            if (GenericDB.isset(filter['='])) {
                if (numeric_key) {
                    ret_object.query[0] = '#column = ?';
                } else {
                    ret_object.query[0] = 'LOWER(#column) = LOWER(CAST( ? AS TEXT))';
                }
                ret_object.data[0] = filter['='];
                return ret_object;
            }

            if (GenericDB.isset(filter['>='])) {
                ret_object.query.push('#column >= ?');
                ret_object.data.push(filter['>=']);
            } else
            if (GenericDB.isset(filter['>'])) {
                ret_object.query.push('#column > ?');
                ret_object.data.push(filter['>']);
            }

            if (GenericDB.isset(filter['<='])) {
                ret_object.query.push('#column <= ?');
                ret_object.data.push(filter['<=']);
            } else
            if (GenericDB.isset(filter['<'])) {
                ret_object.query.push('#column < ?');
                ret_object.data.push(filter['<']);
            }

            if (GenericDB.isset(filter['like'])) {
                ret_object.query.push('#column LIKE ?');
                ret_object.data.push(filter['like']);
            }

            if (GenericDB.isset(filter['regex'])) {
                ret_object.query.push('#column REGEXP ?');
                ret_object.data.push(filter['regex']);
            }

            return ret_object;
        };
        var queries = [];
        var data = [];
        for (var i = 0; i < index_filters.length; i++) {
            var numeric_key = String(index_filters[i].value).match(/[^0-9]/) === null;
            var current = getbounds(index_filters[i].value, numeric_key);
            if (current.query.length < 1) {
                continue;
            }
            var query = current.query.join(' AND ');
            query = query.replace(/\#column/g, '"' + index_filters[i].key + '"');
            queries = queries.concat(query);
            data = data.concat(current.data);
        }

        var query_string = '';
        if (queries.length > 0) {
            query_string = ' WHERE ' + queries.join(' AND ');
        }

        if (typeof callback === 'function') {
            db.transaction(function(tx) {
                var final_query = 'SELECT "' + pk_field + '" FROM "' + cache_store + '" ' + query_string;
                final_query = prepare_statement(final_query, data);
                //console.log(final_query);
                tx.executeSql(final_query, [], function(tx, results) {
                    var pks = [], len = results.rows.length;
                    for (var i = 0; i < len; i++) {
                        pks.push(results.rows.item(i)[pk_field]);
                    }

                    if (len < 1) {
                        //console.log(final_query);
                    }

                    callback(pks, tx);
                }, function(tx, e) {
                    //console.log(e);
                });
            });
        }
        return  {
            query: query_string,
            data: data
        };
    };
    /**
     * Perform a search query
     * @param {object} filters
     * @param {function} callback
     * @param {boolean} return_array
     * @returns {undefined}
     */
    this.search = function(filters, callback, return_array) {
        if (typeof return_array === 'undefined') {
            return_array = GenericDB.RETURN_ARRAY;
        } else {
            return_array = !!return_array; // ensure boolean
        }
        if (typeof callback !== 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();
        var sort_order = this_object.gdb_object.sort_order;

        var cache_table = current_store;
        var pk_field = db_keys[cache_table];
        this_object.get_pks(filters, function(all_pks, tx) {

            if (all_pks.length < 1) {
                //console.log('Nothing found in ' + cache_table);
                //console.log(filters);
                GenericDB.afterload();
                callback([], tx);
                return;
            }
            
            var results;
            if (GenericDB.RETURN_ARRAY === return_array) {
                results = [];
            } else {
                results = {};
            }
            
            var finished_forks = 0;

            var slice_size = 10;
            var nr_forks = Math.ceil(all_pks.length / slice_size);
            var skiped = 0;
            var inserted = 0;
            for (var fork = 0; fork <= nr_forks; fork++) {
                (function() {
                    var pks = all_pks.slice(fork * slice_size, fork * slice_size + slice_size);
                    if (pks.length < 1)
                        return;
                    var qm_array = [];
                    for (var i = 0; i < pks.length; i++) {
                        qm_array.push('?');
                    }

                    var data_query = 'SELECT * FROM "' + cache_table + '" WHERE "' + pk_field + '" IN(' + qm_array.join(',') + ')';
                    data_query = prepare_statement(data_query, pks);
                    tx.executeSql(data_query, [], function(tx, data_results) {

                        for (var i = 0, len = data_results.rows.length; i < len; i++) {
                            if (inserted >= this_object.gdb_object.limit_count) {
                                break;
                            }
                            
                            var pk = data_results.rows.item(i)[pk_field];
                            var record = data_results.rows.item(i);

                            try {
                                var extra_data = JSON.parse(record[extra_column]);
                                for (var data_key in extra_data) {
                                    record[data_key] = extra_data[data_key];
                                }
                            } catch (e) {
                            }

                            if (GenericDB.filterMatch(filters, record)) {
                                if (skiped < this_object.gdb_object.limit_start) {
                                    skiped++;
                                } else {
                                    if (GenericDB.RETURN_ARRAY === return_array) {
                                        results.push(record);
                                    } else {
                                        results[pk] = record;
                                    }
                                    inserted++;
                                }
                            }
                            

                        }
                        finished_forks++;
                        
                        if (finished_forks >= nr_forks) {
                            this_object.gdb_object.sort_order = sort_order;
                            callback(this_object.gdb_object.sort_results(results), tx);
                        }

                    }, function(tx, e) {
                        //console.log(e);
                    });
                })();
            }

        });

        return this_object.gdb_object;
    };
    /**
     * Delete Assets that match the filter
     * @param {mixerd} filter the filter
     * @param {function} callback function(success, error)
     */
    this.delete = function(filter, callback) {

        if (typeof callback !== 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();

        var pk_field = db_keys[current_store];
        var cache_table = current_store;
        this_object.search(filter, function(results, tx) {
            if (results.length < 1) {
                GenericDB.afterload();
                callback(true);
                return;
            }
            var pks = [];
            var qm = [];
            for (var i = 0, len = results.length; i < len; i++) {
                pks.push(results[i][pk_field]);
                qm.push('?');
            }

            var delete_query = 'DELETE FROM "' + cache_table + '" WHERE "' + pk_field + '" IN(' + qm.join(',') + ')';
            delete_query = prepare_statement(delete_query, pks);
            //console.log(delete_query);
            tx.executeSql(delete_query, [], function() {
                GenericDB.afterload();
                callback(true);
            }, function(tx, e) {
                //console.log(e);
                GenericDB.afterload();
                callback(false);
            });
        });

        return this_object.gdb_object;
    };
    /**
     * Update records that match the filter with data provided
     * @param {mixed} filter
     * @param {object} data
     * @param {function} callback this callback will recieve the success state
     */
    this.update = function(filter, data, callback) {

        if (typeof callback !== 'function') {
            callback = function() {
            };
        }

        GenericDB.beforeload();

        var pk_field = db_keys[current_store];
        var cache_table = current_store;
        var indexed_data = [];
        var extra_data = {};
        var indexed_qm = [];
        // filter the data into indexed (in the table) and non-indexed (in extra table)
        for (var field in data) {
            var schema = db_schema[cache_table][field];
            if (typeof schema === 'undefined') {
                 extra_data[field] = data[field];
                continue;
            }

            if (schema.pk || schema.unique || schema.index) {
                indexed_data.push({
                    key: field,
                    value: data[field]
                });
                indexed_qm.push('?');
            } else {
                extra_data[field] = data[field];
            }
        }

        this_object.search(filter, function(results, tx) {
            var sql_data = [];
            var prepared_statements = [];
            for (var i = 0; i < indexed_data.length; i++) {
                prepared_statements.push('"' + indexed_data[i].key + '" = ?');
                sql_data.push(indexed_data[i].value);
            }

            prepared_statements.push('"' + extra_column + '" = ?');
            sql_data.push(JSON.stringify(extra_data));

            var qm = [];
            var pks = [];
            for (var i = 0, len = results.length; i < len; i++) {
                qm.push('?');
                pks.push(results[i][pk_field]);
            }

            var update_query = 'UPDATE "' + cache_table + '" SET ' + prepared_statements.join(',')
                    + ' WHERE "' + db_keys[cache_table] + '" IN(' + qm.join(',') + ')';
            //console.log(update_query);
            update_query = prepare_statement(update_query, sql_data.concat(pks));
            tx.executeSql(update_query, [], function(tx, e) {
                //console.log(e);
                GenericDB.afterload();
                callback(true);

            });
        });

        return this_object.gdb_object;
    };
}
