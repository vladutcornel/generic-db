<?php

/**
 * Scripts
 *
 * @author Adrian Cornel <cornel@scoalaweb.com>
 */
class ClientSide {

    protected static $files = array();
    protected static $extension = 'js';
    static public $root = __DIR__;
    static public $namespace = '\\';

    /**
     * Prevent instantiation of this class
     */
    private function __construct() {
        
    }

    /**
     *
     * @param string $name
     * @param path $file
     * @return boolean success
     */
    public static function register($name, $file = null) {
        //var_dump($name, $file);
        if (is_null($file)) {
            $fileName = static::$root . DS . static::$extension . DS . $name;

            if (\is_dir($fileName)) {
                if (\file_exists($fileName . DS . 'builder.php')) {
                    $file = $fileName . DS . 'builder.php';

                    include_once $file;
                }
            } else if (file_exists($fileName . '.' . static::$extension)) {
                $file = $fileName . '.' . static::$extension;
            } else if (file_exists($fileName . '.php')) {
                $file = $fileName . '.php';
            } else if (file_exists($fileName)) {
                $file = $fileName;
            }
        }

        if (!$file) {
            return false;
        }

        $name = str_replace(array('/', '\\'), '\\', $name);
        $file = str_replace(array('/', '\\'), DS, $file);

        $class = static::$namespace . '\\' . static::$extension . '\\' . $name . '\\builder';
        if (class_exists($class) && method_exists($class, 'init')) {
            $class::init();
        }

        static::$files[$name] = $file;

        return true;
    }

    public static function lineComment($comment) {
        $out = "\r\n/*";

        for ($i = 0; $i < 80; $i ++) {
            $out.='*';
        }
        $out.= "\r\n * " . str_replace('*/', '* /', $comment) . "\r\n ";

        for ($i = 0; $i < 80; $i ++) {
            $out.='*';
        }
        return $out . "*/\r\n";
    }

    /**
     * Return the merged file contents
     * @return string
     */
    public static function get($only = false) {
        $out = '';
        if ($only && isset(static::$files[$only])) {
            $out.= static::renderFile(static::$files[$only], $only);
        } else {

            while ($elem = each(static::$files)) {
                $out.= static::renderFile($elem['value'], $elem['key']);
            }
        }
        return $out;
    }

    /**
     * Get the 
     * @return type
     */
    public static function getFiles() {
        return array_keys(static::$files);
    }

    /**
     * Merge the provided files and return their contents
     * @param array $files
     * @return string The merged JS source
     */
    public static function merge($files) {
        $out = '';
        $class = get_called_class();
        array_walk($files, function($file) use (&$out, $class) {
            $out.= $class::renderFile($file);
        });

        return $out;
    }

    /**
     * Return the contents of the file. Generate for generators
     * @param type $file
     * @param type $name
     * @return type
     */
    public static function renderFile($file, $name = null) {
        //var_dump($name, $file);
        $out = '';
        if (is_null($name)) {
            $name = basename($name);
        }

        if (preg_match('#^http(s?)\://#i', $file)) {


            $out.= static::lineComment($file);

            $out.= @file_get_contents($file);

            return $out;
        }
        if (!file_exists($file)) {

            $out.="\n// File $file was not found \n";
            return $out;
        }

        if ('php' == static::extension($file)) {

            $out.= static::lineComment($name);

            ob_start();
            include_once $file;
            $fileContent = ob_get_clean();

            $builder = static::$namespace . '\\' . static::$extension . '\\' . $name . '\\builder';
            //var_dump($builder, class_exists($builder), method_exists($builder, 'build'));
            if (class_exists($builder) && method_exists($builder, 'build')) {
                $out.= $builder::build();
            } else {
                $out.= $fileContent;
            }

            return $out;
        } else {
            $out.= static::lineComment(basename($file));
        }

        $out.= "\r\n";
        $out.= file_get_contents($file);
        return $out;
    }

    public static function extension($path) {
        return pathinfo($path, PATHINFO_EXTENSION);
    }

}
